<?php require_once ("login-header.php");?>
    <div class="page-program-learning">
        <div class="main-container">
            <div class="main-wrapper d-flex flex-wrap justify-content-between">
                <?php require_once ("componentsPHP/success-sidebar.php");?>
                <div class="content">
                    <div class="test-result-block d-flex flex-wrap">
                        <div class="test-result-image-wrapper">
                            <div class="test-result-image">
                                <img src="images/success-cup.png">
                            </div>
                            <div class="test-result">Тест сдан!</div>
                        </div>
                        <div class="test-result-content">
                            <div class="result-block-title">Тестирование по курсу <br>Тайм-менеджемент завершено!</div>
                            <div class="result-block-text"><span class="result-block-time">Время прохождения теста:</span> 18:25</div>
                            <div class="result-block-sub-text">Вы можете улучшить Ваш результат.</div>
                            <div class="progress-checkmark-wrapper">
                                <div class="progress-checkmark">Проходной бал: 65%</div>
                                <div class="progress-triangle"></div>
                            </div>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 85%; background-color: #008c3d; !important;"  aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="progress-result">Ваш прогресс: 85%</div>
                        </div>
                    </div>

                    <div class="test-result-block d-flex flex-wrap">
                        <div class="test-result-image-wrapper">
                            <div class="test-result-image">
                                <img src="images/success-cup.png">
                            </div>
                            <div class="test-result test-result-fail">Тест не сдан!</div>
                        </div>
                        <div class="test-result-content">
                            <div class="result-block-title">Тестирование по курсу <br>Тайм-менеджемент завершено!</div>
                            <div class="result-block-text"><span class="result-block-time">Время прохождения теста:</span> 18:25</div>
                            <div class="result-block-sub-text">Вы можете улучшить Ваш результат.</div>
                            <div class="progress-checkmark-wrapper">
                                <div class="progress-checkmark">Проходной бал: 65%</div>
                                <div class="progress-triangle"></div>
                            </div>
                            <div class="progress progress-fail">
                                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 25%; background-color: #e92f10; !important;"  aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="progress-result progress-result-fail">Ваш прогресс: 25%</div>
                        </div>
                    </div>

                    <div class="test-result-block d-flex flex-wrap">
                        <div class="test-result-image-wrapper">
                            <div class="test-result-image">
                                <img src="images/success-cup.png">
                            </div>
                            <div class="test-result">Тест сдан!</div>
                        </div>
                        <div class="test-result-content">
                            <div class="result-block-title">Тестирование по курсу <br>Тайм-менеджемент завершено!</div>
                            <div class="result-block-text"><span class="result-block-time">Время прохождения теста:</span> 18:25</div>
                            <div class="result-block-sub-text">Вы можете улучшить Ваш результат.</div>
                            <div class="progress-checkmark-wrapper">
                                <div class="progress-checkmark">Проходной бал: 65%</div>
                                <div class="progress-triangle"></div>
                            </div>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 85%; background-color: #008c3d; !important;"  aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="progress-result">Ваш прогресс: 85%</div>
                            <div class="progress-result-button">
                                <a href="">Улучшить результат</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require_once ("footer.php");?>