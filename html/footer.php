<div class="footer">
    <div class="main-container">
        <div class="footer-wrapper d-flex flex-wrap">
            <div class="footer-main d-flex flex-wrap">
                <div class="content">
                    <div class="footer-title">Заголовок текста в футере</div>
                    <div class="text huy mCustomScrollbar" data-mcs-theme="rounded">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                        reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
<!--                        <div class="footer-circle"></div>-->
                    </div>
                </div>
                <div class="contacts">
                    <div class="footer-title">Контакты</div>
                    <div class="single-contact d-flex justify-content-between align-items-center">
                        <span class="social-image d-flex align-items-center justify-content-center"><i class="fas fa-phone"></i></span><a href="tel:+380 (68) 457-99-07">+380 (68) 457-99-07</a>
                    </div>
                    <div class="single-contact d-flex justify-content-between align-items-center">
                        <span class="social-image d-flex align-items-center justify-content-center"><i class="fas fa-envelope"></i></span><a href="">example@gmail.com</a>
                    </div>
                    <div class="single-contact d-flex justify-content-between align-items-center">
                        <span class="social-image d-flex align-items-center justify-content-center"><i class="fas fa-map-marker-alt"></i></span><a href="">example@gmail.com</a>
                    </div>
                </div>
                <div class="social">
                    <div class="footer-title">Мы в соц. сетях</div>
                    <div class="social-wrapper d-flex flex-wrap">
                        <a href="" class="single-social d-flex justify-content-center align-items-center"><i class="fab fa-facebook-f"></i></a>
                        <a href="" class="single-social d-flex justify-content-center align-items-center"><i class="fab fa-instagram"></i></a>
                        <a href="" class="single-social d-flex justify-content-center align-items-center"><i class="fab fa-facebook-f"></i></a>
                        <a href="" class="single-social d-flex justify-content-center align-items-center"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="main-container">
            <div class="footer-bottom-wrapper d-flex flex-wrap justify-content-between">
                <div class="content">Все права защищены © 2018 «Монополия»</div>
                <div class="content">Технологии <a href="">«ШТУРМАН»</a></div>
            </div>
        </div>
    </div>
</div>