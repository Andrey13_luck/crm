<?php require_once ("login-header.php"); ?>
    <div class="page-single-question">
        <div class="main-container">
            <div class="main-wrapper d-flex flex-wrap justify-content-between">
                <?php require_once ("componentsPHP/info-sidebar.php"); ?>
                <div class="content">
                    <?php require ("componentsPHP/card-single-question.php"); ?>
                </div>
            </div>
        </div>
    </div>
<?php require_once ("footer.php"); ?>