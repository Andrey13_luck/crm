<?php require_once ("login-header.php"); ?>
<div class="test-page">
    <div class="main-container">
        <div class="main-wrapper d-flex flex-wrap justify-content-between">
            <?php require_once ("componentsPHP/sidebar.php"); ?>
            <div class="content">
                <div class="title">Тестирование</div>
                <div class="text">
                    Тут располагаются обязательные тесты
                    не входящие ни в одну категорию обучения.
                </div>
                <div class="test-wrapper">
                    <?php for($i = 0; $i < 4; $i++) { ?>
                        <div class="single-test">
                            <div class="test-title">Длинное-длинное название тестирования </div>
                            <div class="test-date">Необходимо сдать до 29.02.2019</div>
                            <div class="test-button">
                                <a href="">Начать Тестирование</a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>



<?php require_once ("footer.php"); ?>
