<?php require_once ("login-header.php"); ?>
    <div class="asq-question-page">
        <div class="main-container">
            <form class="ask-form" action="" method="">
                <div class="title">Задать вопрос</div>
                <div class="input-wrapper">
                    <div class="input-title">Электронная почта:</div>
                    <input type="text" name="" placeholder="example@gmail.com">
                </div>
                <div class="input-wrapper">
                    <div class="input-title">Телефонный номер:</div>
                    <input type="text" name="" placeholder="+380 ( _ _ ) _ _ _-_ _-_ _">
                </div>
                <div class="input-wrapper">
                    <div class="input-title">Вопрос:</div>
                    <textarea type="text" name="" rows="5" placeholder="Напишите свой вопрос..."></textarea>
                </div>
                <div class="submit-button">
                    <input type="submit" value="Отправить" name="">
                </div>
            </form>
        </div>
    </div>
<?php require_once ("footer.php"); ?>