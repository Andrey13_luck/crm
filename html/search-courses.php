<?php require_once ("header.php"); ?>
    <div class="search-courses-page">
        <div class="main-container">
            <div class="main-title d-flex justify-content-between align-items-center">Все новости<span class="title-line"></span></div>
            <div class="news-wrapper d-flex flex-wrap">
                <?php for($i = 0; $i < 9; $i++) {
                        require ("componentsPHP/news-card.php");
                 } ?>
                <div class="bottom-line"></div>
            </div>
            <?php require_once ("componentsPHP/pagination.php");?>
        </div>
    </div>
<?php require_once ("footer.php"); ?>