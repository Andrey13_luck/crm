<?php require_once ("header.php"); ?>
    <div class="about-us-page">
        <div class="main-container">
            <div class="main-title d-flex justify-content-between align-items-center">О нас<span class="title-line"></span></div>
            <div class="single-post d-flex flex-wrap justify-content-between align-items-center">
                <div class="image">
                    <img src="images/about-us.jpg">
                </div>
                <div class="content d-flex flex-wrap align-items-center">
                    <div class="title">История</div>
                    <div class="text">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                    </div>
                </div>
            </div>
            <div class="single-post d-flex flex-wrap justify-content-between align-items-center">
                <div class="image about-us-image-order">
                    <img src="images/about-us-1.jpg">
                </div>
                <div class="content about-us-content-order d-flex flex-wrap align-items-center">
                    <div class="title">Миссия</div>
                    <div class="text">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim.
                    </div>
                </div>
            </div>
            <div class="video">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/1La4QzGeaaQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>
    </div>
<?php require_once ("footer.php"); ?>