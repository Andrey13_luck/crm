<?php require_once ("login-header.php"); ?>
    <div class="training-program-page">
        <div class="main-container">
            <div class="main-wrapper d-flex flex-wrap justify-content-between">
                <div class="sidebar-training">
                    <div class="title">Кадровый рост</div>
                    <?php for($i = 0; $i < 5; $i++) { ?>
                        <a href="" class="single-program">
                            Программы обучения в рамках
                            программы «Школа руководителя»
                        </a>
                    <?php } ?>
                </div>
                <div class="content">
                    <div class="message-block d-flex">
                        <div class="image">
                            <img src="images/warning.png">
                        </div>
                        <div class="text">
                            Добрый день, <span class="person-name">Константин Константинович</span><br>
                            Вам назначена программа обучения в рамках программы «Школа руководителя», которую
                            необходимо завершить до 31 марта 2018 года.
                        </div>
                    </div>
                    <div class="program-title">Программа обучения на март 2018</div>
                    <hr class="program-title-line">
                    <div class="program-wrapper d-flex flex-wrap">
                        <?php for($i = 0; $i < 5; $i++) { ?>
                            <?php require ("componentsPHP/card-program-learning.php"); ?>
                        <?php } ?>
                    </div>
                    <?php require_once ("componentsPHP/pagination.php");?>
                </div>
            </div>
        </div>
    </div>
<?php require_once ("footer.php"); ?>