<?php //get_header() ;?>

<div class="home-page">
    <div class="main-container">
        <div class="courses-wrapper d-flex flex-wrap">
            <?php for($i = 0; $i < 12; $i++) { ?>
                <a href="" class="single-course">
                    <div class="image">
                        <img src="<?php bloginfo("template_url"); ?>/images/course-img.jpg">
                    </div>
                    <div class="line"></div>
                    <div class="card-title">HR - структура</div>
                </a>
            <?php } ?>
        </div>
        <div class="main-title d-flex justify-content-between align-items-center">Заголовок блока<span class="title-line"></span></div>
        <div class="courses-wrapper d-flex flex-wrap">
            <?php for($i = 0; $i < 4; $i++) { ?>
                <a href="" class="single-course">
                    <div class="image">
                        <img src="<?php bloginfo("template_url"); ?>/images/home-card.png">
                    </div>
                    <div class="card-title">Школа продавца</div>
                </a>
            <?php } ?>
        </div>
        <div class="main-title d-flex  flex-wrap justify-content-between align-items-center">
            Наши новости
            <span class="title-line"></span>
            <div class="title-button">
                <a href="">Все новости</a>
            </div>
        </div>
        <div class="news-wrapper d-flex flex-wrap">
            <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $news = new WP_Query(array("post_type" => "news", "posts_per_page" => 3, 'paged' => $paged));
            if ($news->have_posts()) : while ($news->have_posts()) : $news->the_post();
            require ("componentsPHP/news-card.php");
            endwhile; else: endif; wp_reset_query(); ?>
        </div>
    </div>
</div>
<?php //get_footer() ;?>

