<!DOCTYPE html>
<html>
<head>
    <title>CRM</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src="js/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/fontawesome-all.js"></script>
    <script type="text/javascript" src="js/jquery.selectric.js"></script>
    <script type="text/javascript" src="js/jquery.ddslick.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/selectric.css">
    <link rel="stylesheet" href="css/cssMain/style.css">
    <style>
        @font-face{
            font-family: RobotoRegular;
            src: url('fonts/Roboto-Regular.ttf') format('truetype');
        }
        @font-face{
            font-family: RobotoItalic;
            src: url('fonts/Roboto-Italic.ttf') format('truetype');
        }
        @font-face{
            font-family: RobotoLight;
            src: url('fonts/Roboto-Light.ttf') format('truetype');
        }
        @font-face{
            font-family: RobotoMedium;
            src: url('fonts/Roboto-Medium.ttf') format('truetype');
        }
        @font-face{
            font-family: RobotoThin;
            src: url('fonts/Roboto-Thin.ttf') format('truetype');
        }
        @font-face{
            font-family: RobotoBold;
            src: url('fonts/Roboto-Bold.ttf') format('truetype');
        }
        @font-face{
            font-family: RobotoBlack;
            src: url('fonts/Roboto-Black.ttf') format('truetype');
        }
    </style>
</head>
<body>



<nav class="navbar navbar-custom-class">
    <div class="logo-mobile">
        <a href=""><img src="images/logo-main.png" alt=""></a>
    </div>
    <button class="navbar-toggler navbar-toggler-right collapsed burger-button" type="button" data-toggle="collapse" data-target="#header_id">
        <div class="burger-wrapper">
            <span class="burger-span"></span>
            <span class="burger-span"></span>
            <span class="burger-span"></span>
        </div>
    </button>
    <div class="header-wrapper collapse navbar-collapse" id="header_id">
        <div class="main-container">
            <div class="header-content d-flex flex-wrap justify-content-between">
                <div class="main-logo d-flex align-items-center">
                    <a href=""><img src="images/logo-main.png" alt=""></a>
                </div>
                <form class="registration d-flex align-items-center">
                    <div class="input-wrapper">
                        <input type="text" name="" placeholder="Логин">
                    </div>
                    <div class="input-wrapper">
                        <input type="text" name="" placeholder="Пароль">
                    </div>
                    <div class="submit">
                        <input type="submit" name="" value="Войти">
                    </div>
                </form>
            </div>
        </div>
        <div class="menu-wrapper d-flex align-items-center justify-content-between">
            <div class="header-container-menu d-flex justify-content-between align-items-center">
                <ul class="menu d-flex flex-wrap">
                    <li><a href="">Новости</a></li>
                    <li><a href="">О Нас</a></li>
                    <li><a href="">Контакты</a></li>
                </ul>

                <div class="search-wrapper">
                    <div class="search d-flex">
                        <input type="search" id="search" placeholder="Поиск курсов..." />
                        <span class="icon-search d-flex justify-content-center align-items-center"><i class="fa fa-search"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
