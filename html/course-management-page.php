<?php require_once ("login-header.php"); ?>
    <div class="course-manag-page">
        <div class="main-container">
            <div class="main-wrapper d-flex flex-wrap justify-content-between">
                <?php require_once ("componentsPHP/success-sidebar.php");?>
                <div class="content">
                    <div class="title">Курс по тайм-менеджменту</div>
                    <div class="task-block d-flex justify-content-center align-items-center">
                        <div class="image">
                            <img src="images/clock.png">
                        </div>
                        <div class="text">нУЖНО ПРОЙти</div>
                    </div>

                    <?php require_once ("componentsPHP/course-title.php");?>

                    <div class="main-content wysiwyg-wrapper">
                        <div class="main-content-wrapper">
                            <div class="main-content-title">Длинное-длинное название заголовка блока</div>
                            <hr class="main-content-hr">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                            <div class="video">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/1La4QzGeaaQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                            <div class="test-title">Самотестирование</div>
                            <div class="answer-wrapper">
                                <label for="answer-1" class="markup-add-job-form d-flex align-items-center">
                                    <input type="radio" id="answer-1" name="contact">
                                    <span class="add-job-mark"></span>
                                    <div class="answer-text">Ответ 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</div>
                                </label>

                                <label for="answer-2" class="markup-add-job-form d-flex align-items-center">
                                    <input type="radio" id="answer-2" name="contact">
                                    <span class="add-job-mark"></span>
                                    <div class="answer-text"> Ответ 2. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</div>
                                </label>

                                <label for="answer-3" class="markup-add-job-form d-flex align-items-center">
                                    <input type="radio" id="answer-3" name="contact">
                                    <span class="add-job-mark"></span>
                                    <div class="answer-text">Ответ 3. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</div>
                                </label>

                                <label for="answer-4" class="markup-add-job-form d-flex align-items-center">
                                    <input type="radio" id="answer-4" name="contact">
                                    <span class="add-job-mark"></span>
                                    <div class="answer-text">Ответ 4. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</div>
                                </label>
                            </div>
                            <div class="test-yourself-button">
                                <a href="">Проверить себя</a>
                            </div>

                        </div>
                        <hr class="end-content">
                        <div class="main-content-wrapper">
                            <div class="main-content-title">Длинное-длинное название заголовка блока</div>
                            <hr class="main-content-hr">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                            <img src="images/timemanage.jpg">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
                            </p>
                            <div class="test-title">Самотестирование</div>
                            <div class="answer-wrapper">

                                <div class="single-checkbox d-flex align-items-center">
                                    <input type="checkbox" class="form-checkbox" id="check">
                                    <label class="check-label d-flex" for="check">
                                        <div class="check-custom-wrapper d-flex justify-content-center">
                                            <div class="check-back">
                                                <div class="check-checkmark"><i class="fas fa-check"></i></div>
                                            </div>
                                        </div>
                                    </label>
                                    <div class="check-title">Excepteur sint occaecat cupidatat non proident, sunt in culpa</div>
                                </div>

                                <div class="single-checkbox d-flex align-items-center">
                                    <input type="checkbox" class="form-checkbox" id="check-2">
                                    <label class="check-label d-flex" for="check-2">
                                        <div class="check-custom-wrapper d-flex justify-content-center">
                                            <div class="check-back">
                                                <div class="check-checkmark"><i class="fas fa-check"></i></div>
                                            </div>
                                        </div>
                                    </label>
                                    <div class="check-title">Excepteur sint occaecat cupidatat non proident, sunt in culpa</div>
                                </div>

                                <div class="single-checkbox d-flex align-items-center">
                                    <input type="checkbox" class="form-checkbox" id="check-3">
                                    <label class="check-label d-flex" for="check-3">
                                        <div class="check-custom-wrapper d-flex justify-content-center">
                                            <div class="check-back">
                                                <div class="check-checkmark"><i class="fas fa-check"></i></div>
                                            </div>
                                        </div>
                                    </label>
                                    <div class="check-title">Excepteur sint occaecat cupidatat non proident, sunt in culpa</div>
                                </div>

                                <div class="single-checkbox d-flex align-items-center">
                                    <input type="checkbox" class="form-checkbox" id="check-4">
                                    <label class="check-label d-flex" for="check-4">
                                        <div class="check-custom-wrapper d-flex justify-content-center">
                                            <div class="check-back">
                                                <div class="check-checkmark"><i class="fas fa-check"></i></div>
                                            </div>
                                        </div>
                                    </label>
                                    <div class="check-title">Excepteur sint occaecat cupidatat non proident, sunt in culpa</div>
                                </div>

                            </div>
                            <div class="test-yourself-button">
                                <a href="">Проверить себя</a>
                            </div>
                            <div class="right-answer d-flex align-items-center">
                                <div class="image-answer">
                                    <img src="images/right-answear.png">
                                </div>
                                <div class="text"><span class="success-answer">Верно!</span></div>
                            </div>
                        </div>
                        <hr class="end-content">
                        <div class="main-content-wrapper">
                            <div class="main-content-title">Длинное-длинное название заголовка блока</div>
                            <div class="logo-title">
                                <img src="images/title-logo.png">
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
                            </p>
                            <div class="test-button">
                                <a href="">Самостоятельное пробное тестирование</a>
                            </div>
                            <div class="test-button">
                                <a href="">Экзаменационное тестирование</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require_once ("footer.php"); ?>