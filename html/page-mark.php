<?php require_once ("login-header.php"); ?>
    <div class="page-mark">
        <div class="main-container">
            <div class="main-wrapper d-flex flex-wrap justify-content-between">
                <?php require_once ("componentsPHP/sidebar.php");?>
                <div class="content">
                    <div class="message-block d-flex">
                        <div class="image">
                            <img src="images/warning.png">
                        </div>
                        <div class="text">
                            Информация про тестирование 360 для понимания что это такое!
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
                        </div>
                    </div>
                    <div class="main-title">Оценка 360</div>
                    <div class="main-title-border"></div>
                    <div class="person-wrapper">
                        <?php for($i = 0; $i < 7; $i++) { ?>
                            <a href="" class="single-wrapper d-flex flex-wrap align-items-center">
                                <div class="person-image">
                                    <img src="images/person-image.jpg">
                                </div>
                                <div class="person-content d-flex flex-wrap">
                                    <div class="person-name">Константин Константинович<br>Константинопольский</div>
                                    <div class="person-info-wrapper">
                                        <div class="person-info d-flex flex-wrap">
                                            <div class="single-info">Департамент:</div>
                                            <div class="single-info-position">HR-департамент</div>
                                        </div>
                                        <div class="person-info d-flex flex-wrap">
                                            <div class="single-info">Отдел:</div>
                                            <div class="single-info-position">Отдел подбора персонала</div>
                                        </div>
                                        <div class="person-info d-flex flex-wrap">
                                            <div class="single-info">Должность:</div>
                                            <div class="single-info-position">Менеджер по персоналу</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="person-button">
                                    <button type="button">Начать оценивание</button>
                                </div>
                            </a>
                        <?php } ?>
                        <?php require_once ("componentsPHP/pagination.php");?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require_once ("footer.php"); ?>