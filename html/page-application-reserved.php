<?php require_once ("login-header.php"); ?>
    <div class="page-application-reserved">
        <div class="main-container">
            <div class="main-wrapper d-flex flex-wrap justify-content-between">
                <?php require_once ("componentsPHP/info-sidebar.php"); ?>
                <div class="content">
                    <div class="application-wrapper">
                        <div class="application-text">Добрый день!</div>
                        <div class="application-title">Вы можете подать заявку на участие в направлениях:</div>
                        <div class="proff-wrapper d-flex flex-wrap">
                            <label for="answer-1" class="markup-add-job-form d-flex align-items-center">
                                <input type="radio" id="answer-1" name="contact">
                                <span class="add-job-mark"></span>
                                <div class="answer-text">Кадровый резерв</div>
                            </label>
                            <label for="answer-2" class="markup-add-job-form d-flex align-items-center">
                                <input type="radio" id="answer-2" name="contact">
                                <span class="add-job-mark"></span>
                                <div class="answer-text">Школа руководителя</div>
                            </label>
                            <label for="answer-3" class="markup-add-job-form d-flex align-items-center">
                                <input type="radio" id="answer-3" name="contact">
                                <span class="add-job-mark"></span>
                                <div class="answer-text">Школа продавца</div>
                            </label>
                        </div>
                        <div class="application-main-title">Заявка на участие в программе<br>«Кадровый резерв»</div>
                        <div class="application-main-border"></div>
                        <div class="info-wrapper">
                            <div class="info-single d-flex flex-wrap">
                                <div class="info-title">Фамилия:</div>
                                <div class="info-subject">авто-перенос с базы данных  </div>
                            </div>
                            <div class="info-single d-flex flex-wrap">
                                <div class="info-title">Имя:</div>
                                <div class="info-subject">авто-перенос с базы данных  </div>
                            </div>
                            <div class="info-single d-flex flex-wrap">
                                <div class="info-title">Отчество:</div>
                                <div class="info-subject">авто-перенос с базы данных  </div>
                            </div>
                            <div class="info-single d-flex flex-wrap">
                                <div class="info-title">Отчество:</div>
                                <div class="info-subject">авто-перенос с базы данных  </div>
                            </div>
                            <?php for($i = 0; $i < 4; $i++){?>
                                <div class="info-single d-flex flex-wrap align-items-center">
                                    <div class="info-title">Причина изменения должности:</div>
                                    <div class="info-subject">
                                        <select class="selectric">
                                            <option value="">Введите причину</option>
                                            <option value="">test1</option>
                                            <option value="">test2</option>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="info-button">
                                <a href="">Подать заявку</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require_once ("footer.php"); ?>