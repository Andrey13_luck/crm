<div class="card-single-question">
    <div class="question-title">Длинное название теста</div>
    <hr class="question-hr">
    <div class="question-wrapper d-flex">
        <div class="question-number">3.</div>
        <div class="question-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudm?
        </div>
    </div>
    <div class="answer-wrapper">
        <label for="answer-1" class="markup-add-job-form d-flex align-items-center">
            <input type="radio" id="answer-1" name="contact">
            <span class="add-job-mark"></span>
            <div class="answer-text">Ответ 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</div>
        </label>

        <label for="answer-2" class="markup-add-job-form d-flex align-items-center">
            <input type="radio" id="answer-2" name="contact">
            <span class="add-job-mark"></span>
            <div class="answer-text"> Ответ 2. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</div>
        </label>

        <label for="answer-3" class="markup-add-job-form d-flex align-items-center">
            <input type="radio" id="answer-3" name="contact">
            <span class="add-job-mark"></span>
            <div class="answer-text">Ответ 3. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</div>
        </label>

        <label for="answer-4" class="markup-add-job-form d-flex align-items-center">
            <input type="radio" id="answer-4" name="contact">
            <span class="add-job-mark"></span>
            <div class="answer-text">Ответ 4. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</div>
        </label>
    </div>
    <div class="next-button">
        <button>Далее<i class="fas fa-chevron-right"></i></button>
    </div>
    <?php require_once ("question-pagination.php"); ?>
</div>