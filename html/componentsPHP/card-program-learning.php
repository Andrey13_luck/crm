<a href="" class="single-program-learning">
    <div class="image d-flex justify-content-center align-items-center">
        <img src="images/programm-1.png">
    </div>
    <div class="card-content d-flex justify-content-center">
        <div class="content-image">
            <img src="images/checkmark.png">
        </div>
        <div class="content-text">
            Пройдено
        </div>
    </div>
    <div class="card-text">Тайм-менеджемент</div>
    <div class="card-result">Ваш результат: <span class="result-number">91%</span></div>
</a>

<a href="" class="single-program-learning">
    <div class="image d-flex justify-content-center align-items-center">
        <img src="images/programm-1.png">
    </div>
    <div class="card-content card-content-warning d-flex justify-content-center">
        <div class="content-image">
            <img src="images/checkmark-warning.png">
        </div>
        <div class="content-text">
            Не Пройдено
        </div>
    </div>
    <div class="card-text">Тайм-менеджемент</div>
    <div class="card-result">Ваш результат: <span class="result-number result-number-warning">15%</span></div>
</a>

<a href="" class="single-program-learning">
    <div class="image d-flex justify-content-center align-items-center">
        <img src="images/programm-1.png">
        <div class="order-button">
            <button href="">Заказать тест</button>
        </div>
        <div class="black-layer"></div>
    </div>
    <div class="card-content card-content-necessary card-content-warning d-flex justify-content-center">
        <div class="content-image">
            <img src="images/checkmark-warning.png">
        </div>
        <div class="content-text">
            Не Пройдено
        </div>
    </div>
    <div class="card-text">Тайм-менеджемент</div>
    <div class="card-result">Ваш результат: <span class="result-number result-number-necessary">91%</span></div>
</a>