<a href="" class="news-card">
    <div class="image">
        <img src="images/news-card.jpg">
        <div class="plus d-flex justify-content-center align-items-center">
            <img src="../images/plus.png">
        </div>
    </div>
    <div class="date">12.06.2018</div>
    <div class="text">Длинное-предлинное название новости в две строки</div>
    <div class="more-button">
        <button>подробнее<i class="fas fa-chevron-right"></i></button>
    </div>
</a>