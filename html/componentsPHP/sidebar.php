<div class="sidebar">
    <div class="title">Ваше обучение</div>
    <div class="menu d-flex flex-wrap">
        <ul>
            <li class="d-flex">
                <div class="image">
                    <img src="images/sidebar-img-1.png">
                </div>
                <a href="">Программа обучения</a>
            </li>
            <li class="d-flex">
                <div class="image">
                    <img src="images/sidebar-img-2.png">
                </div>
                <a href="">История обучения</a>
            </li>
<!--            <li class="d-flex">-->
<!--                <div class="image">-->
<!--                    <img src="images/sidebar-img-3.png">-->
<!--                </div>-->
<!--                <a href="">Оценка 360</a>-->
<!--            </li>-->
            <li class="d-flex">
                <div class="image">
                    <img src="images/sidebar-img-4.png">
                </div>
                <a href="">Тестирование</a>
            </li>
            <li class="d-flex">
                <div class="image">
                    <img src="images/sidebar-img-5.png">
                </div>
                <a href="">Результаты обучения</a>
            </li>
<!--            <li class="d-flex">-->
<!--                <div class="image">-->
<!--                    <img src="images/sidebar-img-6.png">-->
<!--                </div>-->
<!--                <a href="">Заказать тест</a>-->
<!--            </li>-->
        </ul>
        <div class="progress-block">
            <div class="progress-title">Динамика обучения</div>
            <div class="text">Ваш прогресс: 75%</div>
            <div class="progress">
                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 75%; background-color: #008c3d !important;"  aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="progress-button">
            <a href="">кАДРОВыЙ РОст</a>
        </div>
    </div>
</div>