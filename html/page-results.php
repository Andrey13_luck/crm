<?php require_once ("login-header.php"); ?>
<div class="page-results">
    <div class="main-container">
        <div class="main-wrapper d-flex flex-wrap justify-content-between">
            <?php require_once ("componentsPHP/sidebar.php");?>
            <div class="content">
                <div class="page-title">Результаты за июнь</div>
                <div class="page-title-border"></div>
                <div class="table-wrapper">
                    <div class="title-block d-flex">
                        <div class="title-cell d-flex align-items-center justify-content-center">История обучения</div>
                        <div class="title-cell d-flex align-items-center justify-content-center">Кол-во попыток</div>
                        <div class="title-cell d-flex align-items-center justify-content-center">Дата и время</div>
                        <div class="title-cell d-flex align-items-center justify-content-center">Бал</div>
                    </div>
                    <div class="content-block d-flex">
                        <div class="content-cell content-cell-title d-flex align-items-center justify-content-center">К</div>
                        <div class="content-cell content-cell-course d-flex align-items-center justify-content-center">Тайм-менеджмент</div>
                        <div class="content-cell d-flex align-items-center justify-content-center">5</div>
                        <div class="content-cell content-cell-time d-flex align-items-center justify-content-center">14.08.2018 <br> затрачено: 19:30</div>
                        <div class="content-cell  table-cell-good d-flex align-items-center justify-content-center">90%</div>
                    </div>
                    <div class="content-block d-flex">
                        <div class="content-cell content-cell-title content-cell-title-orange d-flex align-items-center justify-content-center">Э.Т</div>
                        <div class="content-cell content-cell-course d-flex align-items-center justify-content-center">Длинное-длинное название курса
                            менеджмента в две строчки</div>
                        <div class="content-cell d-flex align-items-center justify-content-center">3</div>
                        <div class="content-cell content-cell-time d-flex align-items-center justify-content-center">09.08.2018 <br> затрачено:  14:28</div>
                        <div class="content-cell table-cell-good d-flex align-items-center justify-content-center">86%</div>
                    </div>

                    <div class="content-block d-flex">
                        <div class="content-cell content-cell-title content-cell-title-blue d-flex align-items-center justify-content-center">С.П.Т.</div>
                        <div class="content-cell content-cell-course d-flex align-items-center justify-content-center">Заголовок в одну строчку</div>
                        <div class="content-cell d-flex align-items-center justify-content-center">1</div>
                        <div class="content-cell content-cell-time d-flex align-items-center justify-content-center">08.08.2018 <br> затрачено:  18:03</div>
                        <div class="content-cell table-cell-warning d-flex align-items-center justify-content-center">62%</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once ("footer.php"); ?>
