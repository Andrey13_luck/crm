<?php require_once ("login-header.php"); ?>
<div class="course-manag-page-fail">
    <div class="main-container">
        <div class="main-wrapper d-flex flex-wrap justify-content-between">
            <?php require_once ("componentsPHP/success-sidebar.php");?>
            <div class="content">
                <div class="course-fail-block d-flex flex-wrap">
                    <div class="block-image">
                        <img src="images/cup.png">
                    </div>
                    <div class="block-content">
                        <div class="block-title">Курс по Тайм-менеджемент не пройден!</div>
                        <div class="block-text">Количество попыток: <span class="block-attempt">3</span></div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 25%; background-color: #e92f10; !important;"  aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="progress-result">Ваш прогресс: 25%</div>
                        <div class="result-title">История попыток</div>
                        <div class="table-wrapper">
                            <div class="table-line d-flex flex-wrap">
                                <div class="single-table">№1</div>
                                <div class="single-table">14.08.2018<br> время на изучение  19:30</div>
                                <div class="single-table table-result">59%</div>
                            </div>
                            <div class="table-line d-flex flex-wrap">
                                <div class="single-table">№2</div>
                                <div class="single-table">14.08.2018 <br> время на изучение  28:10</div>
                                <div class="single-table table-result">25%</div>
                            </div>
                            <div class="table-line d-flex flex-wrap">
                                <div class="single-table">№2</div>
                                <div class="single-table">14.08.2018 <br> время на изучение 45:50</div>
                                <div class="single-table table-result-pass">85%</div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php require_once ("componentsPHP/course-title.php"); ?>
                <div class="main-content wysiwyg-wrapper">
                    <div class="main-content-wrapper">
                        <div class="main-content-title">Длинное-длинное название заголовка блока</div>
                        <hr class="main-content-hr">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                        <div class="video">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/1La4QzGeaaQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                    </div>
                    <hr class="end-content">
                    <div class="main-content-wrapper">
                        <div class="main-content-title">Длинное-длинное название заголовка блока</div>
                        <hr class="main-content-hr">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                        <img src="images/timemanage.jpg">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
                        </p>
                    </div>
                    <hr class="end-content">
                    <div class="main-content-wrapper">
                        <div class="main-content-title">Обязательное тестирование</div>
                        <div class="logo-title">
                            <img src="images/title-logo.png">
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
                        </p>
                        <div class="test-start-button">
                            <a href="">Начать тест</a>
                        </div>
                        <div class="result-test">Результат: <span class="result-test-score">85%</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once ("footer.php");?>