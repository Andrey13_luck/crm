<?php require_once ("login-header.php") ;?>
<div class="page-all-courses">
    <div class="main-container">
        <div class="main-wrapper d-flex flex-wrap justify-content-between">
            <?php require_once ("componentsPHP/sidebar.php") ;?>
            <div class="content">
                <div class="message-block d-flex">
                    <div class="image">
                        <img src="images/warning.png">
                    </div>
                    <div class="text">
                        Здесь находятся курсы всех структур.
                        Вы можете запросить себе курс и после утверждения администратора Вы сможете пройти
                        заказанный Вами курс
                    </div>
                </div>
                <div class="program-title">Все курсы</div>
                <hr class="program-title-line">
                <div class="program-wrapper d-flex flex-wrap">
                    <?php for($i = 0; $i < 5; $i++) { ?>
                        <?php require ("componentsPHP/card-program-learning.php"); ?>
                    <?php } ?>
                </div>
                <?php require_once ("componentsPHP/pagination.php");?>
            </div>
        </div>
    </div>
</div>
<?php require_once ("footer.php") ;?>