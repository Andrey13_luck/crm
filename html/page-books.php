<?php require_once ("login-header.php");?>
<div class="page-books">
    <div class="main-container">
        <div class="main-wrapper d-flex flex-wrap justify-content-between">
            <?php require_once ("componentsPHP/sidebar.php");?>
            <div class="content">
                <div class="page-title">Библиотека книг</div>
                <div class="page-title-border"></div>
                <div class="books-wrapper d-flex flex-wrap">
                    <?php for ($i = 0; $i < 5; $i++) { ?>
                        <div class="single-book">
                            <div class="book-image">
                                <img src="images/books-image.jpg" alt="">
                            </div>
                            <div class="books-content">
                                <div class="book-text">Воронка продаж в
                                    интернете. Инструменты... </div>
                                <div class="books-button">
                                    <a href=""><i class="fas fa-download"></i>Скачать</a>
                                </div>
                            </div>

                        </div>
                    <?php } ?>
                </div>
                <?php require_once ("componentsPHP/pagination.php")?>
            </div>
        </div>
    </div>
</div>
<?php require_once ("footer.php");?>
