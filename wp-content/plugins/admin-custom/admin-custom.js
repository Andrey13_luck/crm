jQuery(function($) {
    var userID;
    var backInfo;
    var optionVal;
    var i;
    $('#select-user').on("change", function(){
        $('.courses-wrapper input').each(function(){
            $(this).removeAttr('checked');
        });
        userID = $(this).val();
        $.ajax({
            type: "post",
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: "selectedCoursesForUser",
                userID: userID
            },
            success: function(response){
                backInfo = jQuery.parseJSON(response);
                $('.courses-wrapper input').each(function(){
                    optionVal = $(this).val();
                    for(i=0;i<backInfo.length;i++){
                        if(optionVal == backInfo[i]){
                            $(this).attr('checked','checked');
                        }
                    }
                });
            }
        });
    });
});

jQuery(function($) {
    var userID;
    var backInfo;
    var optionVal;
    var i;
    $('#select-user-tests').on("change", function(){
        $('.courses-wrapper input').each(function(){
            $(this).removeAttr('checked');
        });
        userID = $(this).val();
        $.ajax({
            type: "post",
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: "selectedTestsForUser",
                userID: userID
            },
            success: function(response){
                backInfo = jQuery.parseJSON(response);
                $('.courses-wrapper input').each(function(){
                    optionVal = $(this).val();
                    for(i=0;i<backInfo.length;i++){
                        if(optionVal == backInfo[i]){
                            $(this).attr('checked','checked');
                        }
                    }
                });
            }
        });
    });
});



jQuery(function($) {
    var staff;
    var backInfo;
    var optionVal;
    var i;
    $('#select-staff').on("change", function(){
        $('.courses-wrapper-staff-set input').each(function(){
            $(this).removeAttr('checked');
        });
        staff = $(this).val();
        $.ajax({
            type: "post",
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: "selectedCoursesForStaff",
                staff: staff
            },
            success: function(response){
                backInfo = jQuery.parseJSON(response);
                $('.courses-wrapper-staff-set input').each(function(){
                    optionVal = $(this).val();
                    for(i=0;i<backInfo.length;i++){
                        if(optionVal == backInfo[i]){
                            $(this).attr('checked','checked');
                        }
                    }
                });
            }
        });
    });
});

jQuery(function($) {
    var staff;
    var backInfo;
    var optionVal;
    var i;
    $('#select-staff-tests').on("change", function(){
        $('.courses-wrapper-staff-set input').each(function(){
            $(this).removeAttr('checked');
        });
        staff = $(this).val();
        $.ajax({
            type: "post",
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: "selectedTestsForStaff",
                staff: staff
            },
            success: function(response){
                backInfo = jQuery.parseJSON(response);
                $('.courses-wrapper-staff-set input').each(function(){
                    optionVal = $(this).val();
                    for(i=0;i<backInfo.length;i++){
                        if(optionVal == backInfo[i]){
                            $(this).attr('checked','checked');
                        }
                    }
                });
            }
        });
    });
});





jQuery(function($){
    var backInfo;
    var i;
    $('#select-user-asked').on('change', function(){
        $('.asked-courses').html("");
        var postInfo = $(this).serialize();
        $.ajax({
            url: "/wp-admin/admin-ajax.php",
            type: "post",
            data: {
                action: 'askedUsersInfo',
                postInfo: postInfo
            },
            success: function (response) {
                backInfo = jQuery.parseJSON(response);
                for(i=0;i<backInfo['resultsInfo'].length;i++){
                    $('.asked-courses').append(
                        "<form action=\"\" method=\"post\" class=\"form-asked d-flex align-items-center\">" +
                        "<div class=\"asked-course-name margin-class-right\">"+backInfo['courseName'][i]+"</div>" +
                        "<div class=\"margin-class-right\"><input type=\"radio\" class=\"radio-asked\" name=\"accept-decline\" value=\"accept\">Принять</div>" +
                        "<div class=\"margin-class-right\"><input type=\"radio\" class=\"radio-asked\" name=\"accept-decline\" value=\"decline\">Отклонить</div>" +
                        "<input type=\"hidden\" name=\"user_id\" value=\""+backInfo['resultsInfo'][i]['user_id']+"\">" +
                        "<input type=\"hidden\" name=\"course_id\" value=\""+backInfo['resultsInfo'][i]['course_id']+"\">" +
                        "<input type=\"submit\" name=\"submit-accept-decline\" class=\"radio-form-class\" value=\"Применить\" disabled>" +
                        "</form>"
                    );
                }

            }
        });
    });
});

jQuery(function($){
    $(document).on("change", '.radio-asked', function(){
        $(this).parents('.form-asked').find('.radio-form-class').removeAttr('disabled');
    });
});

jQuery(function($) {
    $('#select-staff, #select-user, #select-user-tests, #select-staff-tests, #users-results-select, #reserve-users-select, #select-user-asked').selectize({

    });
});