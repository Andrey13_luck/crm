<?php global $wpdb;
$usersQuery = " SELECT * FROM wp_crm_users ";
$users = $wpdb->get_results($usersQuery);
$urlAdmin = admin_url(); ?>

<h1>Кадровый рост. Результаты</h1>

<div style="margin-bottom: 15px">Выберете пользователя</div>
<form action="" method="post" id="users-results-form" style="max-width: 500px;">
    <select name="user" id="users-results-select" style="opacity: 0;">
        <option value="" selected disabled>Выберете пользователя</option>
        <?php foreach ($users as $value) { ?>
            <option value="<?php echo $value->ID; ?>" <?php if($_GET['userID'] == $value->ID) echo 'selected'; ?>><?php echo $value->full_name; ?></option>
        <?php } ?>
    </select>
    <input type="hidden" name="user_id" class="submit-user-id">
</form>

<?php if(isset($_GET['userID'])) {
    $courseQuery = " SELECT * FROM wp_courses_dependencies_reserve WHERE user_id = %s ";
    $coursesResult = $wpdb->get_results($wpdb->prepare($courseQuery, $_GET['userID']));
    $coursesIDs = [];
    if (!empty($coursesResult)) {
        foreach ($coursesResult as $value) {
            array_push($coursesIDs, $value->course_id);
        }
    } else {
        array_push($coursesIDs, "random-value");
    }
    $counterCourses = 0;
    $courses = new WP_Query(array("post_type" => "courses_reserve", "posts_per_page" => -1, 'post__in' => $coursesIDs));
    if ($courses->have_posts()) : while ($courses->have_posts()) : $courses->the_post();
        $currentID = get_the_ID();
        $queryMax = "SELECT MAX(result) FROM wp_tests_results WHERE user_id = %s AND course_id = %s";
        $maxResult = $wpdb->get_results($wpdb->prepare($queryMax, $_GET['userID'], get_the_ID()));
        $passPercent = get_field("percent_pass"); ?>
        <div class="course-title-results"><?php the_title(); ?></div>
        <div class="value-to-pass">Необходимый результат для успешной сдачи: <?=$passPercent;?></div>
        <?php if($maxResult[0]->{'MAX(result)'} != null){ ?>
            <div class="max-result-value">Максимальный результат ползователя:<span class="max-result-user"> <?=$maxResult[0]->{'MAX(result)'};?></span></div>
        <?php }
        if($maxResult[0]->{'MAX(result)'} == null) { ?>
            <div class="status-course-results">Статус: Нужно пройти!</div>
        <?php }
        elseif($maxResult[0]->{'MAX(result)'} >= $passPercent) { ?>
            <div class="status-course-results">Статус: <span style="color: #007433">Пройдено!</span></div>
        <?php }
        elseif($maxResult[0]->{'MAX(result)'} < $passPercent) { ?>
            <div class="status-course-results">Статус: <span style="color: #e92f10">Не пройдено!</span></div>
        <?php }
        if($maxResult[0]->{'MAX(result)'} != null) {
            $queryTests = "SELECT * FROM wp_tests_results WHERE user_id = %s AND course_id = %s";
            $testsResult = $wpdb->get_results($wpdb->prepare($queryTests, $_GET['userID'], $currentID)); ?>
            <div class="details-results">Подробнее:</div>
            <div class="quantity-results">Количество попыток: <span class="quantity-attempts"><?=count($testsResult);?></span></div>
            <?php $counterNumber = 1;
            foreach($testsResult as $value) {

                $secondsRemaining = $value->time_remaining;
                $minutesTotal = get_field('time_to_pass');
                $finalSeconds = ($minutesTotal*60) - $secondsRemaining;

                $finalResultMinutes = floor($finalSeconds/60);
                $finalResultSeconds = $finalSeconds - $finalResultMinutes*60;

                if($secondsRemaining != "end") {
                    $finalResultSecondsRefactor = ($finalResultMinutes != null || $finalResultSeconds != null) ? ((strlen($finalResultSeconds) == 1) ? "0".$finalResultSeconds : $finalResultSeconds) : "";
                    $finalResultToDisplay = ($finalResultMinutes != null || $finalResultSeconds != null) ? "Затраченное время: " . $finalResultMinutes . ":" . $finalResultSecondsRefactor : "";
                } else {
                    $finalResultToDisplay = "Время истекло";
                }

                $time1970 = strtotime($value->date); ?>
                <div class="table-quantity-results d-flex flex-wrap">
                    <div class="single-quantity-result" style="margin-right: 15px;">Попытка №<?=$counterNumber; ?></div>
                    <div class="single-quantity-result" style="margin-right: 15px;">Дата сдачи: <?=date("d.m.Y", $time1970);?></div>
                    <div class="single-quantity-result" style="margin-right: 15px;"><?=$finalResultToDisplay;?></div>
                    <div class="single-quantity-result" style="margin-right: 15px;">Результат: <?=round($value->result, 1); ?>%</div>
                </div>
                <?php $counterNumber++;
            }
        } ?>
        <br>
        <?php $counterCourses++;
    endwhile; else: endif; wp_reset_query();
    if($counterCourses == 0) { ?>
        <div class="no-posts">
            Нет назначенных курсов !
        </div>
    <?php } ?>

<?php } ?>

<!-- Не переносить этот ДЖС в отдельный файл -->
<script>
    jQuery(function($){
        var currentID;
        $('#users-results-select').on('change', function(){
            currentID = $(this).val();
            $('.submit-user-id').attr('value', currentID);
            $('#users-results-form').attr('action','<?=$urlAdmin?>admin.php?page=reserve-results%2Freserve-results.php&userID='+currentID);
            $('#users-results-form').submit();
        });
    });
</script>