<?php
global $wpdb;

$url = admin_url();


if(isset($_GET['decline'])){
    $queryIDs = "SELECT course_id FROM wp_courses_dependencies_reserve WHERE user_id = %s";
    $resultIDs = $wpdb->get_results($wpdb->prepare($queryIDs, $_GET['userID']));
    $arrIDs = [];
    foreach($resultIDs as $value){
        array_push($arrIDs, $value->course_id);
    }
    $valueToDelete = [];
    $courses = new WP_Query(array("post_type" => "courses_reserve", 'posts_per_page' => -1));
    if ($courses->have_posts()) : while ($courses->have_posts()) : $courses->the_post();
        $currentID = get_the_ID();
        if(in_array($currentID, $arrIDs) == true) {
            array_push($valueToDelete, $currentID);
        }
    endwhile; else: endif; wp_reset_query();

    $valueToDeleteString = implode(",", $valueToDelete);
    $queryDel = "DELETE FROM wp_courses_dependencies_reserve WHERE user_id = %s AND course_id IN ($valueToDeleteString)";
    $wpdb->query($wpdb->prepare($queryDel, $_GET['userID']));

    $deleteQuery = "DELETE FROM wp_reserve_applications WHERE user_id = %s";
    $deleteQueryResult = $wpdb->get_results($wpdb->prepare($deleteQuery, $_GET['userID']));
    unset($_GET['userID']);
}



if(isset($_POST['reserve-submit-courses'])){

    $checkCourses = [];
    foreach ($_POST as $key => $value){
        if($key == "reserve-submit-courses"){
            continue;
        }
        array_push($checkCourses, $value);
        $existsQuery = "SELECT 1 FROM wp_courses_dependencies_reserve WHERE user_id = %s AND course_id = %s";
        $existsResult = $wpdb->get_results($wpdb->prepare($existsQuery, $_GET['userID'], $value));
        if(empty($existsResult)){
            $insertQuery = "INSERT INTO wp_courses_dependencies_reserve (user_id, course_id) VALUES (%s, %s)";
            $deleteQueryResult = $wpdb->get_results($wpdb->prepare($insertQuery, $_GET['userID'], $value));
        }
    }


    $queryIDs = "SELECT course_id FROM wp_courses_dependencies_reserve WHERE user_id = %s";
    $resultIDs = $wpdb->get_results($wpdb->prepare($queryIDs, $_GET['userID']));
    $arrIDs = [];

    foreach($resultIDs as $value){
        array_push($arrIDs, $value->course_id);
    }

    $finalCoursesIDs = array_diff($arrIDs, $checkCourses);

    foreach($finalCoursesIDs as $value){
        $queryDel = " DELETE FROM wp_courses_dependencies_reserve WHERE user_id = %s AND course_id = %s ";
        $wpdb->query($wpdb->prepare($queryDel, $_GET['userID'], $value));
    }

}

$askedReserve = "SELECT user_id FROM wp_reserve_applications";
$resultsReserve = $wpdb->get_results($askedReserve); ?>

<h1>Кадровый рост. Заявки</h1>

<div class="reserve-select-wrapper">
    <form method="post" action="" id="ask-reserve-form" style="max-width: 500px;">
        <select class="reserve-users-select" id="reserve-users-select" name="user-info" style="opacity: 0;">
            <option value="" selected disabled>Выберете пользователя</option>
            <?php foreach($resultsReserve as $value) {
                $nameQuery = "SELECT full_name FROM wp_crm_users WHERE ID = %s";
                $nameResult = $wpdb->get_results($wpdb->prepare($nameQuery, $value->user_id)); ?>
                <option value="<?php echo $value->user_id; ?>" <?php if($_GET['userID'] == $value->user_id) echo "selected"; ?>><?php echo $nameResult[0]->full_name; ?></option>
            <?php } ?>
        </select>
    </form>
</div>

<?php if(isset($_GET['userID'])) { ?>
    <div class="reserve-applications-all">
        <?php $currentReserve = "SELECT staff_id, department_id, school_type FROM wp_reserve_applications WHERE user_id = %s";
        $currentReserveResult = $wpdb->get_results($wpdb->prepare($currentReserve, $_GET['userID']));
        $nameQuery = "SELECT full_name, staff, depart FROM wp_crm_users WHERE ID = %s";
        $nameResult = $wpdb->get_results($wpdb->prepare($nameQuery, $_GET['userID'])); ?>
        <div class="reserve_user_name"><?="Текущая должность/отдел: ".$nameResult[0]->staff."/".$nameResult[0]->depart;?></div>
        <?php $staffQuery = "SELECT staff_name FROM wp_crm_staff WHERE ID = %s";
        $staffResult = $wpdb->get_results($wpdb->prepare($staffQuery, $currentReserveResult[0]->staff_id));
        $departQuery = "SELECT departament_name FROM wp_crm_departament WHERE ID = %s";
        $departResult = $wpdb->get_results($wpdb->prepare($departQuery, $currentReserveResult[0]->department_id)); ?>
        <div class="reserve-request-info">
            <div class="current-staff-depart">
                <div class="current-reserve-info"><span class="current-reserve-title">Прграмма:</span> "<?=$currentReserveResult[0]->school_type;?>"<br></div>
                <div class="current-reserve-info"><span class="current-reserve-title">Новая должность:</span> <?=$staffResult[0]->staff_name;?><br></div>
                <div class="current-reserve-info"><span class="current-reserve-title">Новый отдел:</span> <?=$departResult[0]->departament_name;?><br></div>
            </div>
        </div>
    </div>
    <?php if(!isset($_GET['accept']) && !isset($_GET['decline']) && !isset($_POST['reserve-submit-courses'])) { ?>
        <div class="reserve-button-block d-flex flex-wrap">
            <a href="<?=$url;?>admin.php?page=ask-reserve%2Fask-reserve.php&userID=<?=$_GET['userID'];?>&accept=1" class="ask-reserved-status-button">Принять</a>
            <a href="<?=$url;?>admin.php?page=ask-reserve%2Fask-reserve.php&userID=<?=$_GET['userID'];?>&decline=1" class="ask-reserved-status-button">Отклонить</a>
        </div>
    <?php }
}



if(isset($_GET['accept'])){
    $queryIDs = "SELECT course_id FROM wp_courses_dependencies_reserve WHERE user_id = %s";
    $resultIDs = $wpdb->get_results($wpdb->prepare($queryIDs, $_GET['userID']));
    $arrIDs = [];
    foreach($resultIDs as $value){
        array_push($arrIDs, $value->course_id);
    } ?>
    <form method="post" action="<?php echo $url; ?>admin.php?page=ask-reserve%2Fask-reserve.php&userID=<?=$_GET['userID'];?>">
        <div class="courses-courses-select" style="margin-top: 50px;">
            <div class="courses-select-tittle">Выберете курсы</div>
            <div class="courses-wrapper d-flex flex-wrap">
                <?php $courses = new WP_Query(array("post_type" => "courses_reserve", 'posts_per_page' => -1));
                if ($courses->have_posts()) : while ($courses->have_posts()) : $courses->the_post();
                    $currentID = get_the_ID();
                    $checked = false;
                    if(in_array($currentID, $arrIDs) == true) { $checked = true; } ?>
                    <label class="courses-checkbox"><input type="checkbox" value="<?php echo $currentID; ?>" name="course<?php echo $currentID; ?>" <?php if($checked == true) echo "checked"; ?>><?php the_title(); ?></label>
                <?php endwhile; else: endif; wp_reset_query(); ?>
            </div>
        </div>
        <input type="submit" name="reserve-submit-courses" class="ask-reserved-status-button" value="Назначить">
    </form>
<?php } ?>



<script>
    jQuery(function($){
        $('.reserve-users-select').on('change', function(){
            var userID = $(this).val();
            $('#ask-reserve-form').attr('action','<?php echo $url; ?>admin.php?page=ask-reserve%2Fask-reserve.php&userID='+userID);
            $('#ask-reserve-form').submit();
        });
    });
</script>