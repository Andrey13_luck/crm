<?php global $wpdb;
$usersQuery = " SELECT * FROM wp_crm_users ";
$users = $wpdb->get_results($usersQuery);

$staffQuery = " SELECT * FROM wp_crm_staff ";
$staff = $wpdb->get_results($staffQuery);

if($_POST["set-courses"]){

    $checkCourses = [];
    foreach ($_POST as $key => $value){
        if($key == "user" || $key == "set-courses"){
            continue;
        }
        array_push($checkCourses, $value);
        $existsQuery = "SELECT 1 FROM wp_courses_dependencies WHERE user_id = %s AND course_id = %s";
        $existsResult = $wpdb->get_results($wpdb->prepare($existsQuery, $_POST['user'], $value));
        if(empty($existsResult)){
            $dependenciesQuery = "INSERT INTO wp_courses_dependencies (user_id, course_id, personal_course) VALUES (%s, %s, %s)";
            $wpdb->query($wpdb->prepare($dependenciesQuery, $_POST['user'], $value, "+"));
        }
    }

    $checkDeleteQuery = "SELECT * FROM wp_courses_dependencies WHERE user_id = %s";
    $checkDelete = $wpdb->get_results($wpdb->prepare($checkDeleteQuery, $_POST['user']));
    $checkDelFormat = [];
    foreach($checkDelete as $value){
        array_push($checkDelFormat, $value->course_id);
    }

    $finalCoursesIDs = array_diff($checkDelFormat, $checkCourses);

    foreach($finalCoursesIDs as $value){
        $queryDel = " DELETE FROM wp_courses_dependencies WHERE user_id = %s AND course_id = %s ";
        $wpdb->query($wpdb->prepare($queryDel, $_POST['user'], $value));
    } ?>
    <script>
        jQuery(function($) {
            $('#select-user').change();
        });
    </script>
<?php }

/************************************************************************************/

if($_POST["set-courses-staff"]){
    $checkCourses = [];
    foreach ($_POST as $key => $value){
        if($key == "staff" || $key == "set-courses-staff"){
            continue;
        }
        array_push($checkCourses, $value);

        $neededUsers = "SELECT * FROM wp_crm_users WHERE staff = %s";
        $resultNeededUsers = $wpdb->get_results($wpdb->prepare($neededUsers, $_POST['staff']));

        $IDsStaff = [];
        foreach($resultNeededUsers as $item){
            array_push($IDsStaff, $item->ID);
        }

        foreach($IDsStaff as $item){
            $breaker = false;
            $allUserCourses = "SELECT * FROM wp_courses_dependencies WHERE user_id = %s";
            $allUserCoursesResult = $wpdb->get_results($wpdb->prepare($allUserCourses, $item));

            foreach($allUserCoursesResult as $single){
                if($single->course_id == $value){
                    $breaker = true;
                    break;
                }
            }
            if($breaker == true){
                continue;
            }

            $newCourses = "INSERT INTO wp_courses_dependencies (user_id, course_id) VALUES (%s, %s) ";
            $wpdb->query($wpdb->prepare($newCourses, $item, $value));
        }
    }

    // Deleting
    $neededUsers = "SELECT * FROM wp_crm_users WHERE staff = %s";
    $resultNeededUsers = $wpdb->get_results($wpdb->prepare($neededUsers, $_POST['staff']));
    $IDsStaff = [];
    foreach($resultNeededUsers as $item){
        array_push($IDsStaff, $item->ID);
    }
    foreach($IDsStaff as $item){
        $allUserCourses = "SELECT * FROM wp_courses_dependencies WHERE user_id = %s AND personal_course != %s";
        $allUserCoursesResult = $wpdb->get_results($wpdb->prepare($allUserCourses, $item, "+"));
        $checkDelFormat = [];
        foreach($allUserCoursesResult as $value){
            array_push($checkDelFormat, $value->course_id);
        }
        $finalCoursesIDs = array_diff($checkDelFormat, $checkCourses);
        foreach($finalCoursesIDs as $value){
            $queryDel = " DELETE FROM wp_courses_dependencies WHERE user_id = %s AND course_id = %s ";
            $wpdb->query($wpdb->prepare($queryDel, $item, $value));
        }
    } ?>
    <script>
        jQuery(function($) {
            $('#select-staff').change();
        });
    </script>
<?php } ?>






<h1 class="courses-title">Назначить курсы</h1>
<form class="courses-container d-flex flex-wrap" action="" method="post">
    <div class="courses-user-select">
        <select name="user" id="select-user" style="opacity: 0">
            <option value="" selected disabled>Выберете пользователя</option>
            <?php foreach ($users as $value) { ?>
                <option value="<?php echo $value->ID; ?>" <?php if($value->ID == $_POST['user']) echo "selected"; ?>><?php echo $value->full_name; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="courses-courses-select">
        <div class="courses-select-tittle">Выберете курсы</div>
        <div class="courses-wrapper d-flex flex-wrap">
            <?php $courses = new WP_Query(array("post_type" => "courses", 'posts_per_page' => -1));
            if ($courses->have_posts()) : while ($courses->have_posts()) : $courses->the_post();
                $currentID = get_the_ID(); ?>
                <label class="courses-checkbox"><input type="checkbox" value="<?php echo $currentID; ?>" name="course<?php echo $currentID; ?>"><?php the_title(); ?></label>
            <?php endwhile; else: endif; wp_reset_query(); ?>
        </div>
    </div>
    <input class="courses-submit-button" type="submit" value="Назначить" name="set-courses">
</form>




<h1 class="courses-title" style="padding-top: 60px;">Назначить курсы по должностям</h1>
<form class="courses-container d-flex flex-wrap" action="" method="post">
    <div class="courses-user-select">
        <select name="staff" id="select-staff" style="opacity: 0">
            <option value="" selected disabled>Выберете должность</option>
            <?php foreach ($staff as $value) { ?>
                <option value="<?php echo $value->staff_name; ?>" <?php if($value->staff_name == $_POST['staff']) echo "selected"; ?>><?php echo $value->staff_name; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="courses-courses-select">
        <div class="courses-select-tittle">Выберете курсы</div>
        <div class="courses-wrapper-staff-set d-flex flex-wrap">
            <?php $courses = new WP_Query(array("post_type" => "courses", 'posts_per_page' => -1));
            if ($courses->have_posts()) : while ($courses->have_posts()) : $courses->the_post();
                $currentID = get_the_ID(); ?>
                <label class="courses-checkbox"><input type="checkbox" value="<?php echo $currentID; ?>" name="course<?php echo $currentID; ?>"><?php the_title(); ?></label>
            <?php endwhile; else: endif; wp_reset_query(); ?>
        </div>
    </div>
    <input class="courses-submit-button" type="submit" value="Назначить" name="set-courses-staff">
</form>
