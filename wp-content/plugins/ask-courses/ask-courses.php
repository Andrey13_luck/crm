<?php
global $wpdb;


if(isset($_POST['submit-accept-decline'])){
    if($_POST['accept-decline'] == 'accept') {
        $queryInsert = "INSERT INTO wp_courses_dependencies (user_id, course_id) VALUES (%s, %s)";
        $wpdb->query($wpdb->prepare($queryInsert, $_POST['user_id'], $_POST['course_id']));

        $queryDelete = "DELETE FROM wp_asked_courses WHERE course_id = %s AND user_id = %s";
        $wpdb->query($wpdb->prepare($queryDelete, $_POST['course_id'], $_POST['user_id']));

    } elseif($_POST['accept-decline'] == 'decline') {

        $queryDelete = "DELETE FROM wp_asked_courses WHERE course_id = %s AND user_id = %s";
        $wpdb->query($wpdb->prepare($queryDelete, $_POST['course_id'], $_POST['user_id']));

    } ?>
    <script>
        jQuery(function($){
            $('#select-user-asked option').each(function(index){
                if($(this).attr('value') == <?php echo $_POST['user_id']; ?>){
                    $(this).attr('selected','selected');
                    $('#select-user-asked').change();
                }
            });
        });
    </script>
    <?php
}


$askedQuery = "SELECT * FROM wp_asked_courses";
$existsResult = $wpdb->get_results($askedQuery);
$usersIDs = [];
foreach($existsResult as $value){
    if(in_array($value->user_id, $usersIDs) == false){
        array_push($usersIDs, $value->user_id);
    }
}
$usersIDsString = implode(",", $usersIDs);
$usersQuery = " SELECT * FROM wp_crm_users WHERE ID IN ($usersIDsString)";
$users = $wpdb->get_results($usersQuery); ?>

<h1>Запрошеные курсы</h1>
<div class="table-asked">
    <div class="courses-user-select">
        <select name="user" id="select-user-asked" style="opacity: 0">
            <option value="" selected disabled>Выберете пользователя</option>
            <?php foreach($users as $value) { ?>
                <option value="<?php echo $value->ID; ?>"><?php echo $value->full_name; ?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="asked-courses"></div>





