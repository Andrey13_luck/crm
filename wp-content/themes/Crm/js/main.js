jQuery(function($) {
    $('.selectric').selectric();
});

jQuery(function($) {
    $('.page-results .content-cell-course').on('click', function(){
        $(this).find('.tests-results').toggleClass('test-result-visible');
    });
});

/*Height slider transition*/
jQuery(function($) {
    $('.carousel').carousel({
        interval: false
    }).on('slide.bs.carousel', function (e) {
        var nextH = $(e.relatedTarget).height();
        $(this).find('.active.carousel-item').parent().animate({
            height: nextH
        }, 500);
    });
});
/*END Height slider transition*/

jQuery(function($) {
    var backInfo;
    $(".test-content-wrapper").on("submit", function(e){
        $(this).find(".right-answer").hide();
        $(this).find(".right-answer .text span").removeClass('success-answer').html("");
        $(this).find(".right-answer .text span").removeClass('unsuccess-answer').html("");
        $(this).find(".right-answer .image-answer img").attr("src", "");
        e.preventDefault(e);
        var postInfo = $(this).serialize();
        var $form = $(this);
        $.ajax({
            url: "/wp-admin/admin-ajax.php",
            context: $form,
            type: "post",
            data: {
                action : 'questionCourseLearning',
                postInfo : postInfo
            },
            success: function(response){
                backInfo = jQuery.parseJSON(response);
                if(backInfo['result'] == true){
                    this.find(".right-answer").css("display","flex");
                    this.find('.right-answer .text span').addClass('success-answer').html('Верно!');
                    this.find(".right-answer .image-answer img").attr("src", backInfo['path']+"/images/right-answear.png");
                } else {
                    this.find(".right-answer").css("display","flex");
                    this.find('.right-answer .text span').addClass('unsuccess-answer').html('Попробуйте еще раз!');
                    this.find(".right-answer .image-answer img").attr("src", backInfo['path']+"/images/warning.png");
                }
            }
        });

    });
});

jQuery(function($) {
    var radio;
    $(".markup-add-job-form").on("click", function () {
        radio =  $(this).find(".answer-text").html();
        console.log();
        $(".application-main-title .radio-name-course").html(radio);
    });
});


// Main Page Slide Carousel When One Slide appears
jQuery(function($) {
    var slideCount = 0;
    $('#courseSlider .carousel-item').each(function(index){
        if($(this.length != 0)){
            slideCount++;
        }
    });
    if(slideCount == 1){
        $('#courseSlider .carousel-indicators').remove();
        $('#courseSlider .carousel-control-next').remove();
        $('#courseSlider .carousel-control-prev').remove();
    }
});
// End

/*Reserve App*/
jQuery(function($) {
    var check = true;
    var checkStaff;
    var checkDepartment;
    if($('.staff-select').val() == null){
        check = false;
    }
    if($('.department-select').val() == null){
        check = false;
    }
    if(check == true){
        $('.reserve-submit').removeClass('reserve-submit-disabled');
        $('.reserve-submit').removeAttr('disabled');
    }
    
    $('.reserve-select').on('change', function(){
        if($('.staff-select').val() == null){
            checkStaff = false;    
        } else {
            checkStaff = true;
        }
        if($('.department-select').val() == null){
            checkDepartment = false;
        } else {
            checkDepartment = true;
        }
        if(checkDepartment == true && checkStaff == true){
            $('.reserve-submit').removeClass('reserve-submit-disabled');
            $('.reserve-submit').removeAttr('disabled');    
        }
    });
    
});
/*END Reserve App*/