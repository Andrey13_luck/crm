<?php
/**
 * Template Name: Structure Page
 */

session_start();
if(!isset($_SESSION['ID'])) {
    wp_redirect( home_url() );
    die();
}

get_header(); ?>
    <div class="home-page">
        <div class="main-container">
            <div class="courses-wrapper d-flex flex-wrap">
                <?php $structures = new WP_Query(array("post_type" => "structures", "posts_per_page" => 6));
                if ($structures->have_posts()) : while ($structures->have_posts()) : $structures->the_post();?>
                    <a href="<?php the_permalink(); ?>" class="single-course">
                        <div class="image">
                            <?php the_post_thumbnail('medium_large'); ?>
                        </div>
                        <div class="line"></div>
                        <div class="card-title"><?php the_title(); ?></div>
                    </a>
                <?php endwhile; else: endif; wp_reset_query();?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>