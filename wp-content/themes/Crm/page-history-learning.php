<?php
/**
 * Template Name: History Learning Page
 */

session_start();
if(!isset($_SESSION['ID'])) {
    wp_redirect( home_url() );
    die();
}

get_header();
global $wpdb;
$queryInfo = "SELECT * FROM wp_tests_results WHERE user_id = %s";
$infoResult = $wpdb->get_results($wpdb->prepare($queryInfo, $_SESSION["ID"])); ?>

<div class="page-results">
    <div class="main-container">
        <div class="main-wrapper d-flex flex-wrap justify-content-between">
            <?php require_once ("componentsPHP/sidebar.php");?>
            <div class="content">


                <?php $main = 0;
                foreach ($infoResult as $value) {
                    $testType = get_field("tests_type",$value->course_id);
                    if($testType == 'not-main' || $testType == null){
                        continue;
                    }
                    $main++;
                }
                if($main != 0) { ?>
                    <div class="page-title">Профильные</div>
                    <div class="page-title-border"></div>
                    <div class="table-wrapper">
                        <div class="title-block d-flex">
                            <div class="title-cell d-flex align-items-center justify-content-center">История обучения</div>
                            <div class="title-cell d-flex align-items-center justify-content-center">Кол-во попыток</div>
                            <div class="title-cell d-flex align-items-center justify-content-center">Дата и время</div>
                            <div class="title-cell d-flex align-items-center justify-content-center">Бал</div>
                        </div>
                        <?php
                        $alreadyPrinted = [];
                        foreach ($infoResult as $value) {
                            $testType = get_field("tests_type",$value->course_id);
                            if($testType == 'not-main' || $testType == null){
                                continue;
                            }
                            if(in_array($value->course_id,  $alreadyPrinted) == true){
                                continue;
                            }
                            // get percent pass value for course
                            $passPercent = get_field("percent_pass",$value->course_id);
                            $dateCourse = strtotime($value->date); ?>
                            <div class="content-block d-flex">
                                <div class="content-cell content-cell-title d-flex align-items-center justify-content-center"></div>
                                <?php $queryCoursesInfo = "SELECT * FROM `wp_tests_results` WHERE user_id = %s AND course_id = %s ORDER BY ID DESC";
                                $infoCoursesResult = $wpdb->get_results($wpdb->prepare($queryCoursesInfo, $_SESSION["ID"], $value->course_id)); ?>
                                <div class="content-cell content-cell-course d-flex align-items-center justify-content-center">
                                    <?php echo get_the_title($value->course_id); ?>
                                    <div class="tests-results">
                                        <div class="results-title">Последние результаты:</div>
                                        <?php foreach($infoCoursesResult as $item) { ?>
                                            <div class="last-results d-flex">
                                                <div class="date-result">
                                                    <?php echo $item->date; ?>
                                                </div>
                                                <div class="percent-result">
                                                    <?php echo $item->result; ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="chevron">
                                        <img src="<?php bloginfo("template_url"); ?>/images/chevron.png" alt="">
                                    </div>
                                </div>
                                <?php $queryMax = "SELECT MAX(result) FROM wp_tests_results WHERE user_id = %s AND course_id = %s";
                                $queryMaxResult = $wpdb->get_results($wpdb->prepare($queryMax, $_SESSION["ID"], $value->course_id));

                                $queryTime = "SELECT time_remaining FROM wp_tests_results WHERE result = %s AND user_id = %s AND course_id = %s";
                                $timeResult = $wpdb->get_results($wpdb->prepare($queryTime, $queryMaxResult[0]->{'MAX(result)'}, $_SESSION["ID"], $value->course_id));

                                $timeLongValues = [];
                                $counterNull = 0;
                                $counterNotNull = 0;
                                foreach($timeResult as $single){
                                    if($single->time_remaining == null){
                                        $counterNull++;
                                    }
                                    if($single->time_remaining != null){
                                        $counterNotNull++;
                                    }
                                    array_push($timeLongValues, $single->time_remaining);
                                }

                                if($counterNull != null && $counterNotNull != null){
                                    $checkerNull = true;
                                } else {
                                    $checkerNull = false;
                                }
                                if($checkerNull == true){
                                    foreach($timeLongValues as $key => $single){
                                        if($single == null){
                                            unset($timeLongValues[$key]);
                                        }
                                    }
                                }

                                $secondsRemaining = min($timeLongValues);

                                if($secondsRemaining != "end"){
                                    $minutesTotal = get_field('time_to_pass', $value->course_id);
                                    $finalSeconds = ($minutesTotal*60) - $secondsRemaining;
                                    $finalResultMinutes = floor($finalSeconds/60);
                                    $finalResultSeconds = $finalSeconds - $finalResultMinutes*60;
                                    $finalResultSecondsRefactor = ($finalResultMinutes != null || $finalResultSeconds != null) ? ((strlen($finalResultSeconds) == 1) ? "0".$finalResultSeconds : $finalResultSeconds) : "";
                                    $finalResultToDisplay = ($finalResultMinutes != null || $finalResultSeconds != null) ? "затрачено: ".$finalResultMinutes.":".$finalResultSecondsRefactor : "";
                                } else {
                                    $finalResultToDisplay = "Время истекло";
                                } ?>
                                <div class="content-cell d-flex align-items-center justify-content-center"><?php echo count($infoCoursesResult); ?></div>
                                <div class="content-cell content-cell-time d-flex align-items-center justify-content-center"><?php echo date('d.m.Y', $dateCourse); ?> <br> <?=$finalResultToDisplay;?></div>
                                <?php $colorPercent = ($queryMaxResult[0]->{'MAX(result)'} >= $passPercent) ? "#007433" : "#e92f10"; ?>
                                <div class="content-cell d-flex align-items-center justify-content-center" style="color: <?php echo $colorPercent; ?>;"><?php echo $queryMaxResult[0]->{'MAX(result)'}; ?>%</div>
                            </div>
                            <?php array_push($alreadyPrinted, $value->course_id);
                        } ?>
                    </div>
                <?php }



                $notMain = 0;
                foreach ($infoResult as $value) {
                    $testType = get_field("tests_type",$value->course_id);
                    if($testType == 'main' || $testType == null){
                        continue;
                    }
                    $notMain++;
                }
                if($notMain != 0) { ?>
                    <div class="page-title">Не профильные</div>
                    <div class="page-title-border"></div>
                    <div class="table-wrapper">
                        <div class="title-block d-flex">
                            <div class="title-cell d-flex align-items-center justify-content-center">История обучения</div>
                            <div class="title-cell d-flex align-items-center justify-content-center">Кол-во попыток</div>
                            <div class="title-cell d-flex align-items-center justify-content-center">Дата и время</div>
                            <div class="title-cell d-flex align-items-center justify-content-center">Бал</div>
                        </div>
                        <?php
                        $alreadyPrinted = [];
                        foreach ($infoResult as $value) {
                            $testType = get_field("tests_type",$value->course_id);
                            if($testType == 'main' || $testType == null){
                                continue;
                            }
                            if(in_array($value->course_id,  $alreadyPrinted) == true){
                                continue;
                            }
                            // get percent pass value for course
                            $passPercent = get_field("percent_pass",$value->course_id);
                            $dateCourse = strtotime($value->date); ?>
                            <div class="content-block d-flex">
                                <div class="content-cell content-cell-title d-flex align-items-center justify-content-center"></div>
                                <?php $queryCoursesInfo = "SELECT * FROM `wp_tests_results` WHERE user_id = %s AND course_id = %s ORDER BY ID DESC";
                                $infoCoursesResult = $wpdb->get_results($wpdb->prepare($queryCoursesInfo, $_SESSION["ID"], $value->course_id)); ?>
                                <div class="content-cell content-cell-course d-flex align-items-center justify-content-center">
                                    <?php echo get_the_title($value->course_id); ?>
                                    <div class="tests-results">
                                        <div class="results-title">Последние результаты:</div>
                                        <?php foreach($infoCoursesResult as $item) { ?>
                                            <div class="last-results d-flex">
                                                <div class="date-result">
                                                    <?php echo $item->date; ?>
                                                </div>
                                                <div class="percent-result">
                                                    <?php echo $item->result; ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="chevron">
                                        <img src="<?php bloginfo("template_url"); ?>/images/chevron.png" alt="">
                                    </div>
                                </div>
                                <?php $queryMax = "SELECT MAX(result) FROM wp_tests_results WHERE user_id = %s AND course_id = %s";
                                $queryMaxResult = $wpdb->get_results($wpdb->prepare($queryMax, $_SESSION["ID"], $value->course_id));

                                $queryTime = "SELECT time_remaining FROM wp_tests_results WHERE result = %s AND user_id = %s AND course_id = %s";
                                $timeResult = $wpdb->get_results($wpdb->prepare($queryTime, $queryMaxResult[0]->{'MAX(result)'}, $_SESSION["ID"], $value->course_id));

                                $timeLongValues = [];
                                $counterNull = 0;
                                $counterNotNull = 0;
                                foreach($timeResult as $single){
                                    if($single->time_remaining == null){
                                        $counterNull++;
                                    }
                                    if($single->time_remaining != null){
                                        $counterNotNull++;
                                    }
                                    array_push($timeLongValues, $single->time_remaining);
                                }

                                if($counterNull != null && $counterNotNull != null){
                                    $checkerNull = true;
                                } else {
                                    $checkerNull = false;
                                }
                                if($checkerNull == true){
                                    foreach($timeLongValues as $key => $single){
                                        if($single == null){
                                            unset($timeLongValues[$key]);
                                        }
                                    }
                                }

                                $secondsRemaining = min($timeLongValues);

                                if($secondsRemaining != "end"){
                                    $minutesTotal = get_field('time_to_pass', $value->course_id);
                                    $finalSeconds = ($minutesTotal*60) - $secondsRemaining;
                                    $finalResultMinutes = floor($finalSeconds/60);
                                    $finalResultSeconds = $finalSeconds - $finalResultMinutes*60;
                                    $finalResultSecondsRefactor = ($finalResultMinutes != null || $finalResultSeconds != null) ? ((strlen($finalResultSeconds) == 1) ? "0".$finalResultSeconds : $finalResultSeconds) : "";
                                    $finalResultToDisplay = ($finalResultMinutes != null || $finalResultSeconds != null) ? "затрачено: ".$finalResultMinutes.":".$finalResultSecondsRefactor : "";
                                } else {
                                    $finalResultToDisplay = "Время истекло";
                                } ?>
                                <div class="content-cell d-flex align-items-center justify-content-center"><?php echo count($infoCoursesResult); ?></div>
                                <div class="content-cell content-cell-time d-flex align-items-center justify-content-center"><?php echo date('d.m.Y', $dateCourse); ?> <br> <?=$finalResultToDisplay;?></div>
                                <?php $colorPercent = ($queryMaxResult[0]->{'MAX(result)'} >= $passPercent) ? "#007433" : "#e92f10"; ?>
                                <div class="content-cell d-flex align-items-center justify-content-center" style="color: <?php echo $colorPercent; ?>;"><?php echo $queryMaxResult[0]->{'MAX(result)'}; ?>%</div>
                            </div>
                            <?php array_push($alreadyPrinted, $value->course_id);
                        } ?>
                    </div>
                <?php } ?>



            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
