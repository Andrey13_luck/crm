<?php

//  Load styles and scripts

function load_style_script(){
    wp_enqueue_style('main.css', get_template_directory_uri() . '/css/cssMain/style.css');
    wp_enqueue_style('selectric', get_template_directory_uri() . '/css/selectric.css');
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.js');
    wp_enqueue_script('fontawesome-all', get_template_directory_uri() . '/js/fontawesome-all.js');
    wp_enqueue_script('selectricjs', get_template_directory_uri() . '/js/jquery.selectric.js');

    wp_enqueue_script('mainjs', get_template_directory_uri() . '/js/main.js');
}

// Load styles and scripts

add_action("wp_enqueue_scripts", "load_style_script");

/**
 *  Add Thumbnails
 **/
add_theme_support( 'post-thumbnails' );

/**
 * Add Menus
 **/
add_theme_support("menus");

///
// * Register Menu
// /
register_nav_menu("menuHeader", "Header Menu");
register_nav_menu("menuSidebar", "Sidebar Menu");

/*Make ACF Options*/
if( function_exists('acf_add_options_page') ) {
    $args = array(
        'page_title' => 'Options',
        'menu_title' => 'Options',
        //other args
    );
    acf_add_options_page($args);
}
/*End Make ACF Options*/

//Modify TinyMCE editor
function tiny_mce_remove_unused_formats( $initFormats ) {
    // Add block format elements you want to show in dropdown
    $initFormats['block_formats'] = 'Paragraph=p;Heading=h2';
    return $initFormats;
}
add_filter( 'tiny_mce_before_init', 'tiny_mce_remove_unused_formats' );
function tinymce_paste_as_text( $init ) {
    $init['paste_as_text'] = true;
    return $init;
}
add_filter('tiny_mce_before_init', 'tinymce_paste_as_text');
//End Modify TinyMCE editor

/*Email Encoder + ACF*/
//add_filter('acf/load_value', 'eae_encode_emails');
/*End Email Encoder + ACF*/

/*Remove Autocomplete URL*/
remove_filter('template_redirect', 'redirect_canonical');
/*End Remove Autocomplete URL*/

/*Container for content video*/
function alx_embed_html( $html ) {
    return '<div class="wysiwyg-video">' . $html . '</div>';
}
add_filter( 'embed_oembed_html', 'alx_embed_html', 10, 3 );
add_filter( 'video_embed_html', 'alx_embed_html' ); // Jetpack
/*END Container for content video*/

/*
* List of News
*/
add_action("init", "register_news");
function register_news(){
    register_post_type("news", array(
        "public" => true,
        "supports" => array("title", "editor", "thumbnail"),
        "labels" => array(
            "name" => "Список новостей",
            "add_new_item" => "Add"
        ),
    ));
}

/*
* List of Courses
*/
add_action("init", "register_courses");
function register_courses(){
    register_post_type("courses", array(
        "public" => true,
        "supports" => array("title", "editor", "thumbnail"),
        "labels" => array(
            "name" => "Список курсов",
            "add_new_item" => "Add"
        ),
    ));
}

/*
* List of CoursesResrve
*/
add_action("init", "register_courses_reserve");
function register_courses_reserve(){
    register_post_type("courses_reserve", array(
        "public" => true,
        "supports" => array("title", "editor", "thumbnail"),
        "labels" => array(
            "name" => "Кадровый резерв. Курсы",
            "add_new_item" => "Add"
        ),
    ));
}

/*
* List of Structures
*/
add_action("init", "register_structures");
function register_structures(){
    register_post_type("structures", array(
        "public" => true,
        "supports" => array("title", "editor", "thumbnail"),
        "labels" => array(
            "name" => "Список структур",
            "add_new_item" => "Add"
        ),
    ));
}

/*
* List of Custom Courses
*/
add_action("init", "register_custom_courses");
function register_custom_courses(){
    register_post_type("custom-courses", array(
        "public" => true,
        "supports" => array("title", "editor", "thumbnail"),
        "labels" => array(
            "name" => "Список Тестов",
            "add_new_item" => "Add"
        ),
    ));
}

/*
* Education
*/
add_action("init", "register_education");
function register_education(){
    register_post_type("education", array(
        "public" => true,
        "supports" => array("title", "editor", "thumbnail"),
        "labels" => array(
            "name" => "Школа Развития",
            "add_new_item" => "Add"
        ),
    ));
}


/*Custom Default Wordpress Gallery*/
add_filter('post_gallery', 'my_post_gallery', 10, 2);
function my_post_gallery($output, $attr) {
    global $post;
    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }
    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
    ), $attr));
    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image'));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }
    if (empty($attachments)) return '';



    $output .= "";
    // Here's your actual output, you may customize it to your need
    $output .= "<div class=\"wysiwyg-gallery d-flex flex-wrap justify-content-between\">";
    // Now you loop through each attachment
    foreach ($attachments as $id => $attachment) {
        $img = wp_get_attachment_image_src($id, 'medium_large');
        $output .= "<div class=\"wysiwyg-image\">";
        $output .= "<img src=\"{$img[0]}\">";
        $output .= "</div>";
    }
    $output .= "</div>";

    return $output;
}
/*End Custom Default Wordpress Gallery*/

/*Password Generator*/
function generatePassword($length = 8) {
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);
    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }
    return $result;
}
/*END Password Generator*/

/*Set Courses in Admin Panel*/
add_action( 'admin_menu', 'admin_courses' );
function admin_courses() {
    add_menu_page( 'Назначить курсы', 'Назначить курсы', 'manage_options', 'set-courses/set-courses.php', '', 'dashicons-tickets',  5  );
}
/*END Set Courses in Admin Panel*/

/*Set Tests in Admin Panel*/
add_action( 'admin_menu', 'admin_tests' );
function admin_tests() {
    add_menu_page( 'Назначить тесты', 'Назначить тесты', 'manage_options', 'set-tests/set-tests.php', '', 'dashicons-tickets',  6  );
}
/*END Set Tests in Admin Panel*/

/*Ask courses in Admin Panel*/
add_action( 'admin_menu', 'admin_ask_courses' );
function admin_ask_courses() {
    add_menu_page( 'Запрошеные курсы', 'Запрошеные курсы', 'manage_options', 'ask-courses/ask-courses.php', '', 'dashicons-tickets', 6  );
}
/*END Ask courses in Admin Panel*/

/*Kadr Reserve in Admin Panel*/
add_action( 'admin_menu', 'admin_reserve' );
function admin_reserve() {
    add_menu_page( 'Кадровый резерв', 'Кадровый резерв', 'manage_options', 'ask-reserve/ask-reserve.php', '', 'dashicons-tickets', 8  );
}
/*END Kadr Reserve in Admin Panel*/

/*Users results in Admin Panel*/
add_action( 'admin_menu', 'users_results' );
function users_results() {
    add_menu_page( 'Результаты', 'Результаты', 'manage_options', 'users-results/users-results.php', '', 'dashicons-tickets', 7  );
}
/*END Users results in Admin Panel*/

/*Users  reserve results in Admin Panel*/
add_action( 'admin_menu', 'users_reserve_results' );
function users_reserve_results() {
    add_menu_page( 'Кадровый резерв.Результаты', 'Кадровый резерв.Результаты', 'manage_options', 'reserve-results/reserve-results.php', '', 'dashicons-tickets', 9  );
}
/*END Users reserve results in Admin Panel*/

/*Admin styles and scripts*/
function admin_reserve_style_and_scripts() {
    wp_register_style('admin-custom-style', plugins_url('admin-custom/admin-custom.css'));
    wp_enqueue_style('admin-custom-style');
    wp_register_script( 'admin-custom-js', plugins_url('admin-custom/admin-custom.js' ));
    wp_enqueue_script('admin-custom-js');
    wp_register_style('selectize.default', plugins_url('admin-custom/selectize.default.css'));
    wp_enqueue_style('selectize.default');
    wp_register_script( 'selectize', plugins_url('admin-custom/selectize.js' ));
    wp_enqueue_script('selectize');
}
add_action( 'admin_init','admin_reserve_style_and_scripts' );
/*END Admin styles and scripts*/


/*Ajax Selected Courses Admin Panel*/
add_action('wp_ajax_selectedCoursesForUser', 'selectedCoursesForUser');
add_action('wp_ajax_nopriv_selectedCoursesForUser', 'selectedCoursesForUser');
function selectedCoursesForUser(){
    global $wpdb;
    $queryCourses = "SELECT course_id FROM wp_courses_dependencies WHERE user_id = %s ";
    $coursesResult = $wpdb->get_results($wpdb->prepare($queryCourses, $_POST['userID']));

    $arrayBack = [];
    foreach ($coursesResult as $value) {
        array_push($arrayBack, $value->course_id);
    }

    echo json_encode($arrayBack);
    wp_die();
}
/*END Ajax Selected Courses Admin Panel*/

/*Ajax Selected Courses Admin Panel (staff)*/
add_action('wp_ajax_selectedCoursesForStaff', 'selectedCoursesForStaff');
add_action('wp_ajax_nopriv_selectedCoursesForStaff', 'selectedCoursesForStaff');
function selectedCoursesForStaff(){
    global $wpdb;

    $neededUsers = "SELECT * FROM wp_crm_users WHERE staff = %s";
    $resultNeededUsers = $wpdb->get_results($wpdb->prepare($neededUsers, $_POST['staff']));

    $IDsStaff = [];
    foreach($resultNeededUsers as $item){
        array_push($IDsStaff, $item->ID);
    }

    $IDsAll = [];
    $iter = 0;
    foreach($IDsStaff as $item){
        $allUserCourses = "SELECT * FROM wp_courses_dependencies WHERE user_id = %s AND personal_course != %s";
        $allUserCoursesResult = $wpdb->get_results($wpdb->prepare($allUserCourses, $item, "+"));

        $IDsAll[$iter] = [];
        foreach($allUserCoursesResult as $single){
            array_push($IDsAll[$iter], $single->course_id);
        }
        $iter++;
    }

    $finalValues = [];
    $iterCounter = 0;
    foreach($IDsAll as $value){
        if(empty($value)){
            continue;
        }
        if($iterCounter == 0){
            $finalValues = $value;
        } else {
            $helpArr = array_intersect($finalValues, $value);
            $finalValues = array_values($helpArr);
        }
        $iterCounter++;
    }

    $arrayBack = $finalValues;

    echo json_encode($arrayBack);
    wp_die();
}
/*END Ajax Selected Courses Admin Panel (staff)*/


/*Ajax Selected Tests Admin Panel*/
add_action('wp_ajax_selectedTestsForUser', 'selectedTestsForUser');
add_action('wp_ajax_nopriv_selectedTestsForUser', 'selectedTestsForUser');
function selectedTestsForUser(){
    global $wpdb;
    $queryCourses = "SELECT course_id FROM wp_course_dependencies_tests WHERE user_id = %s ";
    $coursesResult = $wpdb->get_results($wpdb->prepare($queryCourses, $_POST['userID']));

    $arrayBack = [];
    foreach ($coursesResult as $value) {
        array_push($arrayBack, $value->course_id);
    }

    echo json_encode($arrayBack);
    wp_die();
}
/*END Ajax Tests Courses Admin Panel*/

/*Ajax Selected Tests Admin Panel (staff)*/
add_action('wp_ajax_selectedTestsForStaff', 'selectedTestsForStaff');
add_action('wp_ajax_nopriv_selectedTestsForStaff', 'selectedTestsForStaff');
function selectedTestsForStaff(){
    global $wpdb;

    $neededUsers = "SELECT * FROM wp_crm_users WHERE staff = %s";
    $resultNeededUsers = $wpdb->get_results($wpdb->prepare($neededUsers, $_POST['staff']));

    $IDsStaff = [];
    foreach($resultNeededUsers as $item){
        array_push($IDsStaff, $item->ID);
    }

    $IDsAll = [];
    $iter = 0;
    foreach($IDsStaff as $item){
        $allUserCourses = "SELECT * FROM wp_course_dependencies_tests WHERE user_id = %s AND personal_test != %s";
        $allUserCoursesResult = $wpdb->get_results($wpdb->prepare($allUserCourses, $item, "+"));

        $IDsAll[$iter] = [];
        foreach($allUserCoursesResult as $single){
            array_push($IDsAll[$iter], $single->course_id);
        }
        $iter++;
    }

    $finalValues = [];
    $iterCounter = 0;
    foreach($IDsAll as $value){
        if(empty($value)){
            continue;
        }
        if($iterCounter == 0){
            $finalValues = $value;
        } else {
            $helpArr = array_intersect($finalValues, $value);
            $finalValues = array_values($helpArr);
        }
        $iterCounter++;
    }

    $arrayBack = $finalValues;

    echo json_encode($arrayBack);
    wp_die();
}
/*END Ajax Selected Tests Admin Panel (staff)*/


/*Question Course Learning*/
/*Ajax Contact Form*/
add_action('wp_ajax_questionCourseLearning', 'questionCourseLearning');
add_action('wp_ajax_nopriv_questionCourseLearning', 'questionCourseLearning');
function questionCourseLearning(){
    $params = [];
    parse_str($_POST['postInfo'], $params);
    $arrayBack = [];

    $corrects = [];
    $answers = [];
    $pathToTheme = get_template_directory_uri();
    $arrayBack['path'] = $pathToTheme;

    foreach($params as $value){
        $array_iter = explode("_", $value);
        if($array_iter[0] == "answer"){
            array_push($answers, $array_iter[1]);
        }
        elseif($array_iter[0] == "correct"){
            array_push($corrects, $array_iter[1]);
        }
    }


    usort($answers, function ($a, $b) {
        return $a > $b;
    });
    usort($corrects, function ($a, $b) {
        return $a > $b;
    });

    $answersResult = implode(",", $answers);
    $correctsResult = implode(",", $corrects);

    if($answersResult == $correctsResult){
        $arrayBack['result'] = true;
    } else {
        $arrayBack['result'] = false;
    }

    echo json_encode($arrayBack);
    wp_die();
}
/*END Question Course Learning*/


/*Ajax Asked Courses Admin Panel*/
add_action('wp_ajax_askedUsersInfo', 'askedUsersInfo');
add_action('wp_ajax_nopriv_askedUsersInfo', 'askedUsersInfo');
function askedUsersInfo(){
    global $wpdb;
    $params = [];
    parse_str($_POST['postInfo'], $params);
    $arrayBack = [];

    $askedQuery = " SELECT * FROM wp_asked_courses WHERE user_id = %s ";
    $askedResult = $wpdb->get_results($wpdb->prepare($askedQuery, $params['user']));
    $arrayBack['resultsInfo'] = $askedResult;

    $idsCoursesArray = [];
    foreach($askedResult as $value){
        $courseName = get_the_title($value->course_id);
        array_push($idsCoursesArray, $courseName);
    }
    $arrayBack['courseName'] = $idsCoursesArray;


    echo json_encode($arrayBack);
    wp_die();
}
/*END Ajax Asked Courses Admin Panel*/


add_action('wp_ajax_timeCounter', 'timeCounter');
add_action('wp_ajax_nopriv_timeCounter', 'timeCounter');
function timeCounter(){
    session_start();
    $_SESSION["testTime"] = $_POST['secondCounter'];

    wp_die();
}

add_action('wp_ajax_timeIsOver', 'timeIsOver');
add_action('wp_ajax_nopriv_timeIsOver', 'timeIsOver');
function timeIsOver(){
    session_start();

    $correctAnswersResult = 0;
    foreach($_SESSION['results'] as $value){
        if($value != 0){
            $correctAnswersResult += $value;
        }
    }
    $percentResult = ($correctAnswersResult*100)/$_POST['countCourses'];

    global $wpdb;
    $resultsQuery = " INSERT INTO wp_tests_results (user_id, course_id, result, time_remaining) VALUES (%s, %s, %s, %s) ";
    $wpdb->query($wpdb->prepare($resultsQuery, $_SESSION["ID"], $_POST['idCourse'], $percentResult, 'end'));

    $coursesLink = get_field("all_courses_page",'options');
    $courseLinkFinal = get_permalink($coursesLink);

    $arrayBack['redirectLink'] = $courseLinkFinal;

    echo json_encode($arrayBack);
    wp_die();
}