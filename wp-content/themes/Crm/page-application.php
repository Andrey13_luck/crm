<?php
/**
 * Template Name: Application Page
 */

global $wpdb;

session_start();
if(!isset($_SESSION['ID'])) {
    wp_redirect( home_url() );
    die();
}

get_header();

if(isset($_POST['cancel_reserve'])){
    $deleteQuery = "DELETE FROM wp_reserve_applications WHERE user_id = %s";
    $deleteResult = $wpdb->get_results($wpdb->prepare($deleteQuery, $_POST['user_id']));
}

if(isset($_POST['reserve-submit'])){
    $ifExistsQuery = "SELECT 1 FROM wp_reserve_applications WHERE user_id = %s AND staff_id = %s AND department_id = %s AND school_type = %s";
    $existsResult = $wpdb->get_results($wpdb->prepare($ifExistsQuery, $_POST['user_id'], $_POST['staff_id'], $_POST['department_id'], $_POST['contact']));
    if($existsResult == null){
        $insertQuery = "INSERT INTO wp_reserve_applications (user_id, staff_id, department_id, school_type) VALUES (%s, %s, %s, %s) ";
        $wpdb->query($wpdb->prepare($insertQuery, $_POST['user_id'], $_POST['staff_id'], $_POST['department_id'], $_POST['contact']));
    }
} ?>

    <div class="page-application">
        <div class="main-container">
            <div class="main-wrapper d-flex flex-wrap justify-content-between">
                <?php require_once ("componentsPHP/sidebar.php"); ?>
                <div class="content">
                    <?php $existsApp = "SELECT 1 FROM wp_reserve_applications WHERE user_id = %s";
                    $existsAppResult = $wpdb->get_results($wpdb->prepare($existsApp, $_SESSION["ID"]));
                    if($existsAppResult == null){ ?>
                        <div class="application-wrapper">
                            <div class="application-text">Добрый день!</div>
                            <div class="application-title">Вы можете подать заявку на участие в направлениях:</div>

                            <form action="" method="post">
                                <input type="hidden" name="user_id" value="<?=$_SESSION['ID'];?>">

                                <div class="proff-wrapper d-flex flex-wrap">
                                    <label for="answer-1" class="markup-add-job-form d-flex align-items-center">
                                        <input type="radio" id="answer-1" name="contact" checked value="Кадровый резерв">
                                        <span class="add-job-mark"></span>
                                        <div class="answer-text">Кадровый резерв</div>
                                    </label>
                                    <label for="answer-2" class="markup-add-job-form d-flex align-items-center">
                                        <input type="radio" id="answer-2" name="contact" value="Школа руководителя">
                                        <span class="add-job-mark"></span>
                                        <div class="answer-text">Школа руководителя</div>
                                    </label>
                                    <label for="answer-3" class="markup-add-job-form d-flex align-items-center">
                                        <input type="radio" id="answer-3" name="contact" value="Школа продавца">
                                        <span class="add-job-mark"></span>
                                        <div class="answer-text">Школа продавца</div>
                                    </label>
                                </div>

                                <?php
                                global $wpdb;
                                $usersQuery = " SELECT * FROM wp_crm_users  WHERE  ID = %s";
                                $userResult = $wpdb->get_results($wpdb->prepare($usersQuery, $_SESSION["ID"])); ?>
                                <div class="application-main-title">Заявка на участие в программе<br>«<span class="radio-name-course">Кадровый резерв</span>»</div>
                                <div class="application-main-border"></div>
                                <?php foreach ($userResult as $value){?>
                                <?php $stringName = $value->full_name;
                                $fio = [];
                                $fio = explode(" ", $stringName); ?>
                                <div class="info-wrapper">
                                    <div class="info-single d-flex flex-wrap">
                                        <div class="info-title">Фамилия:</div>
                                        <div class="info-subject"><?=$fio[0];?></div>
                                    </div>
                                    <div class="info-single d-flex flex-wrap">
                                        <div class="info-title">Имя:</div>
                                        <div class="info-subject"><?=$fio[1];?></div>
                                    </div>
                                    <div class="info-single d-flex flex-wrap">
                                        <div class="info-title">Отчество:</div>
                                        <div class="info-subject"><?=$fio[2];?></div>
                                    </div>
                                    <div class="info-single d-flex flex-wrap">
                                        <div class="info-title">Должность:</div>
                                        <div class="info-subject"><?=$value->staff;?></div>
                                    </div>
                                    <div class="info-single d-flex flex-wrap align-items-center">
                                        <div class="info-title">Отдел кадрового резерва:</div>
                                        <div class="info-subject">
                                            <select class="selectric reserve-select department-select" name="department_id">
                                                <?php $depertQuery = $wpdb->get_results("SELECT * FROM wp_crm_departament"); ?>
                                                <option value="" selected disabled>Выберете отдел</option>
                                                <?php foreach ($depertQuery as $item ) { ?>
                                                    <option value="<?=$item->ID;?>" <?php if($_POST['department_id'] == $item->ID) echo "selected"; ?>><?=$item->departament_name;?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="info-single d-flex flex-wrap align-items-center">
                                        <div class="info-title">Должность кадрового резерва:</div>
                                        <div class="info-subject">
                                            <?php $staffQuery = $wpdb->get_results("SELECT * FROM wp_crm_staff"); ?>
                                            <select class="selectric reserve-select staff-select" name="staff_id">
                                                <option value="" selected disabled>Выберете должность</option>
                                                <?php foreach ($staffQuery as $item ) { ?>
                                                    <option value="<?=$item->ID;?>" <?php if($_POST['staff_id'] == $item->ID) echo "selected"; ?>><?=$item->staff_name;?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="info-button">
                                        <button type="submit" name="reserve-submit" class="reserve-submit reserve-submit-disabled" disabled>Подать заявку</button>
                                    </div>
                                </div>
                            </form>
                            <?php } ?>
                        </div>
                    <?php } elseif(isset($_POST['reserve-submit'])) { ?>
                        <div class="application-wrapper application-success">
                            <div class="application-title">Заяка успешно принята!</div>
                        </div>
                        <form action="" method="post">
                            <input type="hidden" name="user_id" value="<?=$_SESSION['ID'];?>">
                            <input type="submit" name="cancel_reserve" class="cancel-reserve" value="Отменить заявку">
                        </form>
                    <?php } else { ?>
                        <div class="application-wrapper">
                            <div class="application-title">Ваша заявка рассматривается!</div>
                        </div>
                        <form action="" method="post">
                            <input type="hidden" name="user_id" value="<?=$_SESSION['ID'];?>">
                            <input type="submit" name="cancel_reserve" class="cancel-reserve" value="Отменить заявку">
                        </form>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>


<?php get_footer(); ?>