<div class="footer">
    <div class="main-container">
        <div class="footer-wrapper d-flex flex-wrap">
            <div class="footer-main d-flex flex-wrap">
                <div class="content">
                    <?php $footerTitle = get_field("footer_title", "options"); ?>
                    <div class="footer-title"><?php echo $footerTitle; ?></div>
                    <?php $footerText = get_field("footer_text","options");?>
                    <div class="text huy mCustomScrollbar" data-mcs-theme="rounded">
                        <?php echo $footerText ;?>
                    </div>
                </div>
                <div class="contacts">
                    <div class="footer-title">Контакты</div>
                    <?php $footerPhone = get_field("footer_phone", "options");?>
                    <div class="single-contact d-flex justify-content-between align-items-center">
                        <span class="social-image d-flex align-items-center justify-content-center"><i class="fas fa-phone"></i></span><a href="tel:<?php echo $footerPhone;?>"><?php echo $footerPhone;?></a>
                    </div>
                    <?php $footerEmail = get_field("footer_email","options");?>
                    <div class="single-contact d-flex justify-content-between align-items-center">
                            <span class="social-image d-flex align-items-center justify-content-center"><i class="fas fa-envelope"></i></span><a href=""><?php echo $footerEmail;?></a>
                    </div>
                    <?php $footerLocation = get_field("footer_location", "options");?>
                    <div class="single-contact d-flex justify-content-between align-items-center">
                        <span class="social-image d-flex align-items-center justify-content-center"><i class="fas fa-map-marker-alt"></i></span><a href=""><?php echo $footerLocation ;?></a>
                    </div>
                </div>
                <div class="social">
                    <div class="footer-title">Мы в соц. сетях</div>
                    <div class="social-wrapper d-flex flex-wrap">
                        <?php $FacebookLink = get_field("facebook_link","options");
                        $InstaLink = get_field("instagram_link","options");
                        $link1 = get_field("link_1","options");
                        $link2 = get_field("link_2","options");

                         if(! empty($FacebookLink)) { ?>
                        <a href="<?php echo $FacebookLink;?>" class="single-social d-flex justify-content-center align-items-center"><i class="fab fa-facebook-f"></i></a>
                        <?php }
                         if(!empty($InstaLink)) { ?>
                        <a href="<?php echo $InstaLink;?>" class="single-social d-flex justify-content-center align-items-center"><i class="fab fa-instagram"></i></a>
                        <?php }
                         if(!empty($link1)) { ?>
                        <a href="<?php echo $link1;?>" class="single-social d-flex justify-content-center align-items-center"><i class="fab fa-facebook-f"></i></a>
                        <?php }
                         if(!empty($link2)) { ?>
                        <a href="<?php echo $link2;?>" class="single-social d-flex justify-content-center align-items-center"><i class="fab fa-instagram"></i></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="main-container">
            <div class="footer-bottom-wrapper d-flex flex-wrap justify-content-between">
                <div class="content">Все права защищены © 2018 «Монополия»</div>
                <div class="content">Технологии <a href="">«ШТУРМАН»</a></div>
            </div>
        </div>
    </div>
</div>
<?php wp_footer();?>
</body>
</html>