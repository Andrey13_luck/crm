<?php
/**
 * Template Name: All Courses Reserve Page
 */

session_start();
if(!isset($_SESSION['ID'])) {
    wp_redirect( home_url() );
    die();
}

get_header();


$allCoursesPage = get_field('all_courses_page', 'options');
$allCoursesReservePage = get_field('all_courses_reserve_page', 'options');

$currentID = get_the_ID();

if($allCoursesReservePage == $currentID){
    $postType = "courses_reserve";
} else {
    $postType = "courses";
} ?>
    <div class="page-all-courses">
        <div class="main-container">
            <div class="main-wrapper d-flex flex-wrap justify-content-between">
                <?php require_once ("componentsPHP/sidebar.php") ;?>
                <div class="content">

                    <div class="program-title"><?php the_title(); ?></div>
                    <hr class="program-title-line">
                    <div class="program-wrapper d-flex flex-wrap">



                        <?php
                        global $wpdb;
                        if($allCoursesReservePage == $currentID){
                            $courseQuery = " SELECT * FROM wp_courses_dependencies_reserve WHERE user_id = %s ";
                        } else {
                            $courseQuery = " SELECT * FROM wp_courses_dependencies WHERE user_id = %s ";
                        }
                        $coursesResult = $wpdb->get_results($wpdb->prepare($courseQuery, $_SESSION["ID"]));
                        $coursesIDs = [];
                        if (!empty($coursesResult)) {
                            foreach ($coursesResult as $value) {
                                array_push($coursesIDs, $value->course_id);
                            }
                        } else {
                            array_push($coursesIDs, "random-value");
                        }

                        $counterCourses = 0;
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $courses = new WP_Query(array("post_type" => $postType, "posts_per_page" => 6, 'post__in' => $coursesIDs, 'paged' => $paged));
                        if ($courses->have_posts()) : while ($courses->have_posts()) : $courses->the_post();
                            $queryMax = "SELECT MAX(result) FROM wp_tests_results WHERE user_id = %s AND course_id = %s";
                            $maxResult = $wpdb->get_results($wpdb->prepare($queryMax, $_SESSION["ID"], get_the_ID()));
                            $passPercent = get_field("percent_pass");
                            $colorPercent = ($passPercent > $maxResult[0]->{'MAX(result)'}) ? '#e92f10' : '#008c3d'; ?>
                            <a href="<?php the_permalink(); ?>" class="single-program-learning">
                                <div class="image d-flex justify-content-center align-items-center">
                                    <img src="<?php bloginfo("template_url"); ?>/images/programm-1.png">
                                </div>
                                <?php
                                if($maxResult[0]->{'MAX(result)'} == null) { ?>
                                    <div class="card-content card-content-necessary card-content-warning d-flex justify-content-center">
                                        <div class="content-image">
                                            <img src="<?php bloginfo("template_url"); ?>/images/checkmark-warning.png">
                                        </div>
                                        <div class="content-text">
                                            Нужно пройти
                                        </div>
                                    </div>
                                <?php }
                                elseif($maxResult[0]->{'MAX(result)'} >= $passPercent) { ?>
                                    <div class="card-content d-flex justify-content-center">
                                        <div class="content-image">
                                            <img src="<?php bloginfo("template_url"); ?>/images/checkmark.png">
                                        </div>
                                        <div class="content-text">
                                            Пройдено
                                        </div>
                                    </div>
                                <?php }
                                elseif($maxResult[0]->{'MAX(result)'} < $passPercent) { ?>
                                    <div class="card-content card-content-warning d-flex justify-content-center">
                                        <div class="content-image">
                                            <img src="<?php bloginfo("template_url"); ?>/images/checkmark-warning.png">
                                        </div>
                                        <div class="content-text">
                                            Не пройдено
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="card-text"><?php the_title(); ?></div>
                                <?php if($maxResult[0]->{'MAX(result)'} != null){ ?>
                                    <div class="card-result">Ваш результат: <span class="result-number" style="color: <?php echo $colorPercent; ?>"><?php echo round($maxResult[0]->{'MAX(result)'}, 1); ?>%</span></div>
                                <?php } ?>
                            </a>
                            <?php $counterCourses++;
                        endwhile; else: endif; wp_reset_query();
                        if($counterCourses == 0) { ?>
                            <div class="no-posts">
                                Нет курсов для вас !
                            </div>
                        <?php } ?>
                    </div>


                    <div class="pagination">
                        <?php
                        global $wp_query;
                        $big = 999999999; // need an unlikely integer
                        echo paginate_links( array(
                            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                            'format' => '?paged=%#%',
                            'current' => max( 1, get_query_var('paged') ),
                            'total' => $courses->max_num_pages,
                            'prev_text' => "<i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>",
                            'next_text' => "<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>",
                        ));
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>