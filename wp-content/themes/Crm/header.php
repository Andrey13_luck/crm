<?php
global $wpdb;

if(isset($_POST["logout"])){
    session_start();
    unset($_SESSION);
    session_destroy();

    wp_redirect( home_url() );
    die();

} else {
    session_start();
}

if(isset($_POST["authorization-form"])){
    $login = $_POST['form-login'];
    $queryUser = " SELECT * FROM wp_crm_users WHERE login = %s ";
    $result = $wpdb->get_results($wpdb->prepare($queryUser, $login));
    if(!empty($result)){
        $formPassword = $result[0]->password;
        $fullName = $result[0]->full_name;
        $idUser = $result[0]->ID;
        $checked = password_verify($_POST["form-password"], $formPassword);
        if ($checked) {
            session_start();
            $_SESSION = [];
            $_SESSION["ID"] = $idUser;
            $_SESSION["form-login"] = $_POST['form-login'];
            $_SESSION["form-password"] = password_hash($_POST["form-password"], PASSWORD_DEFAULT);
            $_SESSION["full-name"] = $fullName;
        }
    }
}

$allCoursesPage = get_field('all_courses_page', 'options'); ?>


    <!DOCTYPE html>
    <html>
    <head>
        <title>CRM</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <style>
            @font-face{
                font-family: RobotoRegular;
                src: url('<?php bloginfo("template_url"); ?>/fonts/Roboto-Regular.ttf') format('truetype');
            }
            @font-face{
                font-family: RobotoItalic;
                src: url('<?php bloginfo("template_url"); ?>/fonts/Roboto-Italic.ttf') format('truetype');
            }
            @font-face{
                font-family: RobotoLight;
                src: url('<?php bloginfo("template_url"); ?>/fonts/Roboto-Light.ttf') format('truetype');
            }
            @font-face{
                font-family: RobotoMedium;
                src: url('<?php bloginfo("template_url"); ?>/fonts/Roboto-Medium.ttf') format('truetype');
            }
            @font-face{
                font-family: RobotoThin;
                src: url('<?php bloginfo("template_url"); ?>/fonts/Roboto-Thin.ttf') format('truetype');
            }
            @font-face{
                font-family: RobotoBold;
                src: url('<?php bloginfo("template_url"); ?>/fonts/Roboto-Bold.ttf') format('truetype');
            }
            @font-face{
                font-family: RobotoBlack;
                src: url('<?php bloginfo("template_url"); ?>/fonts/Roboto-Black.ttf') format('truetype');
            }
        </style>
        <?php wp_enqueue_script('jquery'); ?>
        <?php wp_head(); ?>
    </head>
<body>




    <nav class="navbar navbar-custom-class">
        <div class="logo-mobile">
            <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo("template_url"); ?>/images/logo-main.png" alt=""></a>
        </div>
        <button class="navbar-toggler navbar-toggler-right collapsed burger-button" type="button" data-toggle="collapse" data-target="#header_id">
            <div class="burger-wrapper">
                <span class="burger-span"></span>
                <span class="burger-span"></span>
                <span class="burger-span"></span>
            </div>
        </button>
        <div class="header-wrapper collapse navbar-collapse" id="header_id">
            <div class="main-container">
                <div class="header-content d-flex flex-wrap justify-content-between">
                    <div class="main-logo d-flex align-items-center">
                        <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo("template_url"); ?>/images/logo-main.png" alt=""></a>
                    </div>

                    <?php if(isset($_SESSION['form-login'])) { ?>
                        <?php $queryUserInfo = " SELECT * FROM wp_crm_users WHERE ID = %s ";
                        $resultInfo = $wpdb->get_results($wpdb->prepare($queryUserInfo, $_SESSION["ID"]));
                        foreach ($resultInfo as $value) { ?>
                            <div class="content-header-wrapper d-flex flex-wrap justify-content-between">
                                <div class="info d-flex flex-wrap">
                                    <div class="info-single d-flex justify-content-between">
                                        <div class="name">Департамент:</div>
                                        <div class="position"><?=$value->depart?></div>
                                    </div>
                                    <div class="info-single d-flex justify-content-between">
                                        <div class="name">Должность:</div>
                                        <div class="position"><?=$value->staff?></div>
                                    </div>
                                    <div class="info-single d-flex justify-content-between">
                                        <div class="name">Отдел:</div>
                                        <div class="position"><?=$value->subdivision?></div>
                                    </div>
                                </div>
                                <div class="login d-flex flex-wrap justify-content-between">
                                    <div class="login-info d-flex flex-wrap">
                                        <div class="name"><?php echo $_SESSION["full-name"]; ?></div>
                                        <?php if($allCoursesPage != null) {
                                            $allCoursesPageLink = get_permalink($allCoursesPage); ?>
                                            <div class="login-button" id="private-office">
                                                <a href="<?php echo $allCoursesPageLink; ?>">перейти в личный кабинет</a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php

                                    $queryImage = "SELECT `photo_link` FROM `wp_crm_photos` WHERE user_id = %s";
                                    $queryResult = $wpdb->get_results($wpdb->prepare($queryImage, $_SESSION["ID"]));

                                    ?>
                                    <div class="login-image">
                                        <div class="wrapper-header-image" style="width: 100%; height: 100%;">
                                            <img src="data:image/jpeg;base64,<?=$queryResult[0]->photo_link;?>">
                                        </div>
                                        <div class="logout d-flex justify-content-center align-items-center">
                                            <form action="" method="post">
                                                <button type="submit" name="logout"><i class="fas fa-sign-out-alt"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php }
                    else { ?>
                        <form class="registration d-flex align-items-center" method="post" action="">
                            <div class="input-wrapper">
                                <input type="text" name="form-login" placeholder="Логин">
                            </div>
                            <div class="input-wrapper">
                                <input type="password" name="form-password" placeholder="Пароль">
                            </div>
                            <div class="submit">
                                <input type="submit" name="authorization-form" value="Войти">
                            </div>
                        </form>
                    <?php } ?>

                </div>
            </div>
            <div class="menu-wrapper d-flex align-items-center justify-content-between">
                <div class="header-container-menu d-flex justify-content-between align-items-center">
                    <?php wp_nav_menu(array("theme_location" => "menuHeader", 'menu_class' => 'menu d-flex flex-wrap')); ?>
                    <div class="search-wrapper">
                        <?php $searchPage = get_field('search_courses', 'options'); ?>
                        <form action="<?php echo $searchPage; ?>" method="get" class="search d-flex">
                            <input type="search" id="search" placeholder="Поиск курсов..." name="search-phrase" value="<?php if(isset($_GET['search-phrase'])) echo $_GET['search-phrase']; ?>">
                            <?php if(isset($_SESSION['form-login'])) { ?>
                                <button type="submit" class="icon-search d-flex justify-content-center align-items-center"><i class="fa fa-search"></i></button>
                            <?php } else { ?>
                                <a href="" data-toggle="modal" data-target="#regWarningModal" class="icon-search d-flex justify-content-center align-items-center"><i class="fa fa-search"></i></a>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </nav>


<?php if(!isset($_SESSION['form-login'])) { ?>
    <div class="register-modal-window">
        <div id="regWarningModal" class="modal" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="authorization-message">
                        Для начала авторизируйтесь!
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
