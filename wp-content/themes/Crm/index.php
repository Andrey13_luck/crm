<?php
/**
 * Template Name: Main Page
 */
get_header(); ?>
    <div class="home-page">
        <div class="main-container">
            <!--Structures-->
            <div class="courses-wrapper d-flex flex-wrap">
                <?php $structures = new WP_Query(array("post_type" => "structures", "posts_per_page" => 12));
                if ($structures->have_posts()) : while ($structures->have_posts()) : $structures->the_post();?>
                    <a href="<?php the_permalink(); ?>" class="single-course">
                        <div class="image">
                            <?php the_post_thumbnail('medium_large'); ?>
                        </div>
                        <div class="line"></div>
                        <div class="card-title"><?php the_title(); ?></div>
                    </a>
                <?php endwhile; else: endif; wp_reset_query();?>
            </div>
            <!--            End Structures-->

            <!--            Courses-->
            <div class="main-title d-flex justify-content-between align-items-center">Школа развития<span class="title-line"></span></div>
            <div class="courses-wrapper d-flex flex-wrap">
                <?php $courses = new WP_Query(array("post_type" => "education", "posts_per_page" => 4));
                if ($courses->have_posts()) : while ($courses->have_posts()) : $courses->the_post();?>
                    <a href="<?php the_permalink(); ?>" class="single-course">
                        <div class="image">
                            <?php the_post_thumbnail('medium'); ?>
                        </div>
                        <div class="line"></div>
                        <div class="card-title"><?php the_title(); ?></div>
                    </a>
                <?php endwhile; else: endif; wp_reset_query();?>
            </div>
            <!--            End Courses-->
            <div class="main-title d-flex  flex-wrap justify-content-between align-items-center">
                Наши новости
                <span class="title-line"></span>
                <?php $newsLink = get_field("our_news","options"); ?>
                <div class="title-button">
                    <a href="<?php echo $newsLink;?>">Все новости</a>
                </div>
            </div>
            <div class="news-wrapper d-flex flex-wrap">
                <?php $news = new WP_Query(array("post_type" => "news", "posts_per_page" => 3));
                if ($news->have_posts()) : while ($news->have_posts()) : $news->the_post();
                    require ("componentsPHP/news-card.php");
                endwhile; else: endif; wp_reset_query(); ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>