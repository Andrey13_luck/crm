<?php global $wpdb;
$coursesSidebarQuery = " SELECT * FROM wp_courses_dependencies WHERE user_id = %s ";
$coursesSidebarResult = $wpdb->get_results($wpdb->prepare($coursesSidebarQuery, $_SESSION["ID"]));
$coursesSidebarIDs = [];
if (!empty($coursesSidebarResult)) {
    foreach ($coursesSidebarResult as $value) {
        array_push($coursesSidebarIDs, $value->course_id);
    }
} else {
    array_push($coursesSidebarIDs, "random-value");
}

$passedGoodCounter = 0;
$testsTotalCounter = 0;
$coursesSidebar = new WP_Query(array("post_type" => "courses", "posts_per_page" => -1, 'post__in' => $coursesSidebarIDs));
if ($coursesSidebar->have_posts()) : while ($coursesSidebar->have_posts()) : $coursesSidebar->the_post();
    $relationTest = get_field('select_related_test');
    if($relationTest == null){
        continue;
    }
    $querySidebarMax = "SELECT MAX(result) FROM wp_tests_results WHERE user_id = %s AND course_id = %s";
    $maxSidebarResult = $wpdb->get_results($wpdb->prepare($querySidebarMax, $_SESSION["ID"], $relationTest[0]));
    $passPercent = get_field("percent_pass", $relationTest[0]);
    $checkPassed = ($maxSidebarResult[0]->{'MAX(result)'} >= $passPercent && $maxSidebarResult[0]->{'MAX(result)'} != null) ? true : false;
    if($checkPassed == true){
        $passedGoodCounter++;
    }
    $testsTotalCounter++;
endwhile; else: endif; wp_reset_query();

if($testsTotalCounter == 0){
    $finalPercent = 0;
}
else{
    $finalPercent = ($passedGoodCounter*100)/$testsTotalCounter;
}

$colorResult = "";
switch (true) {
    case $finalPercent <= 33:
        $colorResult = "#e92f10";
        $colorBack = "#ffa798";
        break;
    case $finalPercent <= 66 && $finalPercent > 33:
        $colorResult = "#ff8400";
        $colorBack = "#ffa800";
        break;
    case $finalPercent > 66:
        $colorResult = "#007433";
        $colorBack = "#bbefd1";
        break;
} ?>
<div class="sidebar">
    <div class="title">Ваше обучение</div>
    <div class="menu d-flex flex-wrap">
        <?php wp_nav_menu(array("theme_location" => "menuSidebar", 'container_class' => 'menu d-flex flex-wrap')); ?>
        <div class="progress-block">
            <div class="progress-title">Динамика обучения</div>
            <div class="text" style="color: <?php echo $colorResult; ?> !important;">Ваш прогресс: <?php echo round($finalPercent, 1); ?>%</div>
            <div class="progress" style="background-color: <?php echo $colorBack; ?> !important;">
                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: <?php echo round($finalPercent, 1); ?>%; background-color: <?php echo $colorResult; ?> !important;"></div>
            </div>
        </div>

        <?php $currentPage = get_queried_object();
        $applicaton = get_field('application_page', 'options');
        if($currentPage->ID != $applicaton) {
            $appLink = get_permalink($applicaton);
            if($appLink != null) { ?>
                <div class="progress-button">
                    <a href="<?= $appLink; ?>">Кадровый рост</a>
                </div>
            <?php }
        } ?>
    </div>
</div>