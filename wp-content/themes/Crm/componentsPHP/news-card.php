<a href="<?php the_permalink(); ?>" class="news-card">
    <div class="image">
        <?php $thumb = get_the_post_thumbnail();
        if($thumb != null) {
            the_post_thumbnail('medium_large');
        } else { ?>
            <img src="<?php bloginfo("template_url"); ?>/images/news-card.jpg">
        <?php } ?>
        <div class="plus d-flex justify-content-center align-items-center">
            <img src="<?php bloginfo("template_url"); ?>/images/plus.png">
        </div>
    </div>
    <div class="date"><?php echo get_the_date("j.n.Y"); ?></div>
    <div class="text"><?php the_title(); ?></div>
    <div class="more-button">
        <button>подробнее<i class="fas fa-chevron-right"></i></button>
    </div>
</a>