<div class="info-sidebar">
    <div class="title">История успеха</div>
    <div class="title-border"></div>


    <div id="demo" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="person-info">
                    <div class="person-image d-flex justify-content-center align-items-center">
                        <img src="images/sidebar-avatar.jpg">
                    </div>
                    <div class="person-text">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="person-info">
                    <div class="person-image d-flex justify-content-center align-items-center">
                        <img src="images/sidebar-avatar.jpg">
                    </div>
                    <div class="person-text">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="person-info">
                    <div class="person-image d-flex justify-content-center align-items-center">
                        <img src="images/sidebar-avatar.jpg">
                    </div>
                    <div class="person-text">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="person-info">
                    <div class="person-image d-flex justify-content-center align-items-center">
                        <img src="images/sidebar-avatar.jpg">
                    </div>
                    <div class="person-text">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
                    </div>
                </div>
            </div>
        </div>
        <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
            <li data-target="#demo" data-slide-to="3"></li>
        </ul>


        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon d-flex justify-content-center align-items-center"><i class="fas fa-angle-left"></i></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon d-flex justify-content-center align-items-center"><i class="fas fa-angle-right"></i></span>
        </a>

    </div>


    <div class="company-block">
        <div class="company-title">Кадровый рост <br>сотрудников в компании</div>
        <div class="company-image">
            <img src="images/company-image.jpg">
        </div>
        <div class="company-text">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </div>
    </div>
    <div class="info-button">
        <a href="">Тестирование «Карьерный компас»</a>
    </div>
    <div class="info-button">
        <a href="">Подать заявку</a>
    </div>
</div>