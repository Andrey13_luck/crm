<?php
/**
 * Template Name: Ask Question Page
 */

if (isset($_POST['ask-question-button'])){
    $email = $_POST['form_email'];
    $phone = $_POST['form_phone'];
    $formMessage = htmlspecialchars($_POST['form_question']);
    $subject = "У Вас новое сообщение из контактной формы";
    $email_to = get_field("ask_page_email", "options");
    $title_name = $subject;
    $headers = [
        'content-type: text/html',
    ];
    $message = "От: ".$email_to." "."<br>"."Телефон: ".$phone."<br>" ."Вопрос: ". $formMessage;
    wp_mail($email_to, $title_name, $message, $headers);
    wp_redirect($_SERVER['REQUEST_URI']);
    exit;
}

get_header(); ?>
    <div class="asq-question-page">
        <div class="main-container">
            <form class="ask-form" action="" method="post">
                <div class="title"><?php the_title(); ?></div>
                <div class="input-wrapper">
                    <div class="input-title">Электронная почта:</div>
                    <input type="text" name="form-email" placeholder="example@gmail.com">
                </div>
                <div class="input-wrapper">
                    <div class="input-title">Телефонный номер:</div>
                    <input type="text" name="form-phone" placeholder="+380 ( _ _ ) _ _ _-_ _-_ _">
                </div>
                <div class="input-wrapper">
                    <div class="input-title">Вопрос:</div>
                    <textarea type="text" name="form-question" rows="5" placeholder="Напишите свой вопрос..."></textarea>
                </div>
                <div class="submit-button">
                    <input type="submit" value="Отправить" name="ask-question-button">
                </div>
            </form>
        </div>
    </div>
<?php get_footer(); ?>