<?php
/**
 * Template Name: News Page
 */
?>
<?php get_header(); ?>
<div class="search-courses-page">
    <div class="main-container">
        <div class="main-title d-flex justify-content-between align-items-center"><?php the_title(); ?><span class="title-line"></span></div>
        <div class="news-wrapper d-flex flex-wrap">
            <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $news = new WP_Query(array("post_type" => "news", "posts_per_page" => 12, 'paged' => $paged));
            if ($news->have_posts()) : while ($news->have_posts()) : $news->the_post();
                require ("componentsPHP/news-card.php");
            endwhile; else: endif; wp_reset_query(); ?>
            <div class="bottom-line"></div>
        </div>
        <?php require_once ("componentsPHP/pagination.php");?>
    </div>
</div>
<?php get_footer(); ?>


