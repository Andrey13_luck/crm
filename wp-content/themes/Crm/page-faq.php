<?php
/**
 * Template Name: Faq Page
 */
session_start();
if(!isset($_SESSION['ID'])) {
    wp_redirect( home_url() );
    die();
}

get_header(); ?>
    <div class="page-faq">
        <div class="main-container">
            <div class="main-wrapper d-flex flex-wrap justify-content-between">
                <?php require_once ("componentsPHP/sidebar.php");?>
                <div class="content">
                    <div class="program-title">Ответы на вопросы</div>
                    <hr class="program-title-line">
                    <div class="accordion-wrapper">
                        <div id="accordion">
                            <?php $faqContent = get_field("faq_content");
                            $questionCounter = 1;
                            foreach ($faqContent as $value){
                                ?>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link collapsed d-flex" data-toggle="collapse" href="#collapseOne<?=$questionCounter;?>">
                                            <?= $value['question_title']; ?>
                                            <div class="card-image"></div>
                                        </a>

                                    </div>
                                    <div id="collapseOne<?= $questionCounter;?>" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <?= $value['question_answer']; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $questionCounter++;
                            } ?>

                            <!--                            <div class="card">-->
                            <!--                                <div class="card-header">-->
                            <!--                                    <a class="card-link collapsed d-flex" data-toggle="collapse" href="#collapseTwo">-->
                            <!--                                        Длинный-длинный вопрос располагающийся в этом поле в одну строчку-->
                            <!--                                        <div class="card-image"></div>-->
                            <!--                                    </a>-->
                            <!---->
                            <!--                                </div>-->
                            <!--                                <div id="collapseTwo" class="collapse" data-parent="#accordion">-->
                            <!--                                    <div class="card-body">-->
                            <!--                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                            <!--                                    </div>-->
                            <!--                                </div>-->
                            <!--                            </div>-->
                            <!---->
                            <!--                            <div class="card">-->
                            <!--                                <div class="card-header">-->
                            <!--                                    <a class="card-link collapsed d-flex" data-toggle="collapse" href="#collapseThree">-->
                            <!--                                        Длинный-длинный вопрос располагающийся в этом поле в одну строчку-->
                            <!--                                        <div class="card-image"></div>-->
                            <!--                                    </a>-->
                            <!---->
                            <!--                                </div>-->
                            <!--                                <div id="collapseThree" class="collapse" data-parent="#accordion">-->
                            <!--                                    <div class="card-body">-->
                            <!--                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                            <!--                                    </div>-->
                            <!--                                </div>-->
                            <!--                            </div>-->
                            <!---->
                            <!--                            <div class="card">-->
                            <!--                                <div class="card-header">-->
                            <!--                                    <a class="card-link collapsed d-flex" data-toggle="collapse" href="#collapseFour">-->
                            <!--                                        Длинный-длинный вопрос располагающийся в этом поле в одну строчку-->
                            <!--                                        <div class="card-image"></div>-->
                            <!--                                    </a>-->
                            <!---->
                            <!--                                </div>-->
                            <!--                                <div id="collapseFour" class="collapse" data-parent="#accordion">-->
                            <!--                                    <div class="card-body">-->
                            <!--                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                            <!--                                    </div>-->
                            <!--                                </div>-->
                            <!--                            </div>-->
                            <!---->
                            <!--                            <div class="card">-->
                            <!--                                <div class="card-header">-->
                            <!--                                    <a class="card-link collapsed d-flex" data-toggle="collapse" href="#collapseFive">-->
                            <!--                                        Краткий вопрос про важные условия-->
                            <!--                                        <div class="card-image"></div>-->
                            <!--                                    </a>-->
                            <!---->
                            <!--                                </div>-->
                            <!--                                <div id="collapseFive" class="collapse" data-parent="#accordion">-->
                            <!--                                    <div class="card-body">-->
                            <!--                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                            <!--                                    </div>-->
                            <!--                                </div>-->
                            <!--                            </div>-->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>