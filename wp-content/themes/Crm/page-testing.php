<?php
/**
 * Template Name: Testing Page
 */

if($_SERVER['REQUEST_URI'] == "/testirovanie" || $_SERVER['REQUEST_URI'] == "/testirovanie/"){
    wp_redirect(  home_url() );
    exit;
}

session_start();
if(!isset($_SESSION['ID'])) {
    wp_redirect( home_url() );
    die();
}

get_header();

$courses = get_field("tests", $_GET['id_course']);

$currentQuestion = 1;
if(isset($_GET['question'])){
    $currentQuestion = $_GET['question'];
}
if(!isset($_GET['question'])){
    unset($_SESSION['results']);
    unset($_SESSION['already-answered']);
    unset($_SESSION['testTime']);
}

if($_SESSION['already-answered'] == null){
    $_SESSION['already-answered'] = [];
}
if($_SESSION['results'] == null){
    $_SESSION['results'] = [];
}

if(isset($_POST['get-answer'])){
    $questionCounter = $currentQuestion - 1;
    $answers = [];
    $correctAnswers = [];
    foreach ($_POST as $key => $value) {
        if($key == "get-answer" || $key == "questionCurrent"){
            continue;
        }
        $currentIter = explode("-", $key);
        if($currentIter[0] == "answer"){
            array_push($answers, $value);
        }
        elseif($currentIter[0] == "correct"){
            array_push($correctAnswers, $value);
        }
    }


    if(count($correctAnswers) == 1){
        /***************/
        if(count($answers) == 1){
            $answersFinal = implode(",", $answers);
            $correctsFinal = implode(",", $correctAnswers);
            if($answersFinal == $correctsFinal){
                $_SESSION['results']['question-number-'.$questionCounter] = 1;
            } else {
                $_SESSION['results']['question-number-'.$questionCounter] = 0;
            }
        }
        /***************/
        elseif(count($answers) > 1){
//            $correctOnlyOneCounter = 0;
//            $incorrectOnlyOneCounter = 0;
//            foreach($answers as $value){
//                if(in_array($value, $correctAnswers) == true){
//                    $correctOnlyOneCounter++;
//                } else {
//                    $incorrectOnlyOneCounter++;
//                }
//            }
//            if($correctOnlyOneCounter == 0){
//                $_SESSION['results']['question-number-'.$questionCounter] = 0;
//            }
//            elseif($correctOnlyOneCounter != 0 && $incorrectOnlyOneCounter == 0) {
//                $_SESSION['results']['question-number-'.$questionCounter] = 1;
//            }
//            elseif($correctOnlyOneCounter != 0 && $incorrectOnlyOneCounter != 0) {
//                $wasteVariantsResult = (($incorrectOnlyOneCounter*100)/$_POST['totalAnswers'])/100;
//                $_SESSION['results']['question-number-'.$questionCounter] = 1 - $wasteVariantsResult;
//            }

            // If 2 variants checked (even if one is correct), we get result 0 (if you want result proportional% of correct answers, then uncomment code above)
            $_SESSION['results']['question-number-'.$questionCounter] = 0;
        }
        /***************/
        else {
            $_SESSION['results']['question-number-'.$questionCounter] = 0;
        }
        /***************/
    }


    elseif(count($correctAnswers) > 1) {
        /***************/
        if(count($answers) == 1){
            if(in_array($answers[0], $correctAnswers) == true){
                $correctOneFromMany = round((100/count($correctAnswers))/100, 2);
                $_SESSION['results']['question-number-'.$questionCounter] = $correctOneFromMany;
            }
            else {
                $_SESSION['results']['question-number-'.$questionCounter] = 0;
            }
        }
        /***************/
        elseif(count($answers) > 1){
            /****/
            $correctManyCounter = 0;
            $incorrectManyCounter = 0;
            foreach($answers as $value){
                if(in_array($value, $correctAnswers) == true){
                    $correctManyCounter++;
                } else {
                    $incorrectManyCounter++;
                }
            }
            /****/
            if(count($answers) < count($correctAnswers)){
                if($incorrectManyCounter == 0){
                    $goodVariantsResult = round(count($answers)/count($correctAnswers), 2);
                    $_SESSION['results']['question-number-'.$questionCounter] = $goodVariantsResult;
                }
                elseif($correctManyCounter == 0){
                    $_SESSION['results']['question-number-'.$questionCounter] = 0;
                }
                elseif ($correctManyCounter != 0 && $incorrectManyCounter != 0){
                    $goodVariantsResult = round((count($answers) - $incorrectManyCounter)/count($correctAnswers), 2);
                    $_SESSION['results']['question-number-'.$questionCounter] = $goodVariantsResult;
                }
            }
            /****/
            elseif(count($answers) > count($correctAnswers)){
                if(count($correctAnswers) == $correctManyCounter){
                    $wasteVariantsResult = (($incorrectManyCounter*100)/$_POST['totalAnswers'])/100;
                    $_SESSION['results']['question-number-'.$questionCounter] = 1 - $wasteVariantsResult;
                }
                elseif(count($correctAnswers) > $correctManyCounter){
                    $goodVariantsResult = round((count($answers) - $incorrectManyCounter)/count($correctAnswers), 2);
                    $_SESSION['results']['question-number-'.$questionCounter] = $goodVariantsResult;
                }
                elseif($correctManyCounter == 0){
                    $_SESSION['results']['question-number-'.$questionCounter] = 0;
                }
            }
            /****/
            elseif(count($answers) == count($correctAnswers)){
                if($correctManyCounter == count($correctAnswers)){
                    $_SESSION['results']['question-number-'.$questionCounter] = 1;
                }
                else {
                    $goodVariantsResult = round((count($correctAnswers) - $incorrectManyCounter)/count($correctAnswers), 2);
                    $_SESSION['results']['question-number-'.$questionCounter] = $goodVariantsResult;
                }
            }
            /****/
        }
        /***************/
        else {
            $_SESSION['results']['question-number-'.$questionCounter] = 0;
        }
        /***************/
    }

    array_push($_SESSION['already-answered'], $_POST['questionCurrent']);
}

if($_GET['question'] > count($courses)){
    $correctAnswersResult = 0;
    foreach($_SESSION['results'] as $value){
        if($value != 0){
            $correctAnswersResult += $value;
        }
    }
    $percentResult = ($correctAnswersResult*100)/count($courses);

    global $wpdb;
    $resultsQuery = " INSERT INTO wp_tests_results (user_id, course_id, result, time_remaining) VALUES (%s, %s, %s, %s) ";
    $wpdb->query($wpdb->prepare($resultsQuery, $_SESSION["ID"], $_GET['id_course'], $percentResult, $_SESSION['testTime']));


    $coursesLink = get_field("all_courses_page",'options');
    $courseLinkFinal = get_permalink($coursesLink); ?>
    <script>
        jQuery(function($) {
            window.location.replace("<?php echo $courseLinkFinal; ?>");
        });
    </script>
<?php }

$testingPage = get_field("testing_page", "options"); ?>
<div class="test-single-question-page">
    <div class="main-container">
        <div class="main-wrapper d-flex flex-wrap justify-content-between">
            <?php require_once ("componentsPHP/sidebar.php"); ?>
            <div class="content">

                <div class="card-single-question">





                    <?php
                    $time = get_field('time_to_pass', $_GET['id_course']);
                    if($time != null){
                        if(!isset($_SESSION["testTime"])) {
                            $secondQuantity = $time * 60;
                        } else {
                            $secondQuantity = $_SESSION["testTime"];
                        } ?>
                        <script>
                            /*Countdown auction*/
                            jQuery(function($) {
                                var backInfo;
                                var minutes;
                                var seconds;
                                var secondCounter = parseInt('<?php echo $secondQuantity; ?>');
                                var countCourses = parseInt('<?php echo count($courses); ?>');
                                var idCourse = parseInt('<?php echo $_GET['id_course']; ?>');
                                var x = setInterval(function() {
                                    minutes = Math.floor(secondCounter / 60);
                                    $('.time .minutes-number').html(minutes);

                                    seconds = secondCounter%60;

                                    seconds = ""+seconds+"";
                                    if(seconds.length == 1){
                                        seconds = "0"+seconds;
                                    }

                                    $('.time .seconds-number').html(seconds);


                                    secondCounter--;
                                    if (secondCounter == 0) {
                                        clearInterval(x);
                                        $.ajax({
                                            url: "/wp-admin/admin-ajax.php",
                                            type: 'post',
                                            data: {
                                                action: 'timeIsOver',
                                                countCourses: countCourses,
                                                idCourse: idCourse
                                            },
                                            success: function (response) {
                                                backInfo = jQuery.parseJSON(response);
                                                window.location.replace(backInfo['redirectLink']);
                                            }
                                        });
                                    }

                                    else {
                                        $.ajax({
                                            url: "/wp-admin/admin-ajax.php",
                                            type: 'post',
                                            data: {
                                                action: 'timeCounter',
                                                secondCounter: secondCounter,

                                            },
                                            success: function (response) {}
                                        });
                                    }

                                }, 1000);
                            });
                            /*END Countdown auction*/
                        </script>
                    <?php } ?>

                    <?php $title = get_the_title($_GET['id_course']); ?>
                    <div class="question-title"><?php echo $title ?></div>
                    <hr class="question-hr">
                    <?php if($time != null) { ?>
                        <div class="time-text">Оставшееся время:</div>
                        <div class="time-wrapper d-flex justify-content-center">
                            <div class="time">
                                <span class="minutes-number"></span>:<span class="seconds-number"></span>
                                <div class="time-image">
                                    <img src="<?php bloginfo("template_url"); ?>/images/timer.png" alt="">
                                </div>
                            </div>

                        </div>
                    <?php } ?>

                    <form action="<?php echo $testingPage; ?>?id_course=<?php echo $_GET['id_course']; ?>&question=<?php echo $currentQuestion + 1; ?>" method="post">
                        <?php $counter = 1;
                        foreach($courses as $value) {
                            if($counter == $currentQuestion) {
                                if(!in_array($currentQuestion, $_SESSION['already-answered'])){ ?>
                                    <div class="question-wrapper d-flex">
                                        <div class="question-number"><?php echo $counter ?>.</div>
                                        <div class="question-text"><?php echo $value['question']; ?></div>
                                    </div>
                                    <div class="answer-wrapper">
                                        <?php $answerCounter = 1;
                                        foreach($value["variants"] as $item) { ?>
                                            <label for="answer-<?php echo $answerCounter; ?>" class="markup-add-job-form d-flex align-items-center">
                                                <input type="checkbox" id="answer-<?php echo $answerCounter; ?>" name="answer-<?php echo $answerCounter; ?>" value="<?php echo $answerCounter; ?>">
                                                <span class="add-job-mark"></span>
                                                <div class="answer-text"><?php echo $item["answer"]; ?></div>
                                                <?php if($item['correct_answer'] == true) { ?>
                                                    <input name="correct-<?php echo $answerCounter; ?>" type="hidden" value="<?php echo $answerCounter; ?>">
                                                <?php } ?>
                                            </label>
                                            <?php $answerCounter++;
                                        } ?>
                                    </div>
                                    <input type="hidden" name="totalAnswers" value="<?php echo count($value["variants"]); ?>">
                                    <div class="next-button">
                                        <button type="submit" name="get-answer">Далее<i class="fas fa-chevron-right"></i></button>
                                    </div>
                                <?php } else {
                                    echo "<div class='already-answered'>Вы уже ответили на этот вопрос !</div>";
                                }
                            }
                            $counter++;
                        } ?>
                        <input type="hidden" name="questionCurrent" value="<?php echo $currentQuestion; ?>">
                        <?php if($currentQuestion > count($courses)) { ?>
                            <div class="the-end" id="end-course-test">Конец</div>
                        <?php } ?>
                    </form>


                    <div class="question-pagination d-flex flex-wrap">
                        <?php for($i = 1; $i <= count($courses); $i++) { ?>
                            <a href="<?php echo $testingPage ?>?id_course=<?php echo $_GET['id_course']; ?>&question=<?php echo $i; ?>" class="<?php if($i == $currentQuestion) echo "pagination-active"; ?>"><?php echo $i; ?></a>
                        <?php } ?>
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
