<?php
/**
 * Template Name: Search Courses Page
 */

global $wpdb;

if(!isset($_GET['search-phrase'])){
    wp_redirect( home_url() );
    die();
}

if(isset($_GET['asked-course-id']) && isset($_GET['asked-user-id'])){
    $checkAskedCourse = " SELECT 1 FROM wp_asked_courses WHERE user_id = %s AND course_id = %s ";
    $checkCourseResult = $wpdb->get_results($wpdb->prepare($checkAskedCourse, $_GET['asked-user-id'], $_GET['asked-course-id']));
    if($checkCourseResult == null){
        $askedQuery = " INSERT INTO wp_asked_courses (user_id, course_id) VALUES (%s, %s) ";
        $wpdb->query($wpdb->prepare($askedQuery, $_GET['asked-user-id'], $_GET['asked-course-id']));
    }
}

get_header();

$coursesSearchQuery = " SELECT ID FROM wp_posts WHERE post_title LIKE %s AND post_type = 'courses' ";
$coursesSearchResult = $wpdb->get_results($wpdb->prepare($coursesSearchQuery, "%".$_GET['search-phrase']."%"));
$coursesSearchIDs = [];
foreach ($coursesSearchResult as $value) {
    array_push($coursesSearchIDs, $value->ID);
}
if(empty($coursesSearchIDs)){
    array_push($coursesSearchIDs, "random-value");
} ?>



    <div class="page-all-courses">
        <div class="main-container">
            <div class="main-wrapper d-flex flex-wrap justify-content-between">
                <?php require_once ("componentsPHP/sidebar.php") ;?>
                <div class="content">
                    <div class="message-block d-flex">
                        <div class="image">
                            <img src="<?php bloginfo("template_url"); ?>/images/warning.png">
                        </div>
                        <div class="text">
                            Здесь находятся курсы всех структур.
                            Вы можете запросить себе курс и после утверждения администратора Вы сможете пройти
                            заказанный Вами курс
                        </div>
                    </div>
                    <div class="program-title"><?php the_title(); ?></div>
                    <hr class="program-title-line">
                    <div class="program-wrapper d-flex flex-wrap">

                        <?php $counterCourses = 0;
                        $dependenceQuery = "SELECT * FROM wp_courses_dependencies WHERE user_id = %s";
                        $dependenceResult = $wpdb->get_results($wpdb->prepare($dependenceQuery, $_SESSION['ID']));
                        $dependenceIDs = [];
                        foreach($dependenceResult as $value){
                            array_push($dependenceIDs, $value->course_id);
                        }

                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $courses = new WP_Query(array("post_type" => "courses", "posts_per_page" => 6, 'post__in' => $coursesSearchIDs, 'paged' => $paged));
                        if ($courses->have_posts()) : while ($courses->have_posts()) : $courses->the_post();

                            $currentID = get_the_ID();
                            $maxResult = "not-null-random-value";
                            $passPercent = get_field("percent_pass");
                            if(in_array($currentID, $dependenceIDs) == true){
                                $queryMax = "SELECT MAX(result) FROM wp_tests_results WHERE user_id = %s AND course_id = %s";
                                $maxResult = $wpdb->get_results($wpdb->prepare($queryMax, $_SESSION["ID"], $currentID));
                                $colorPercent = ($passPercent > $maxResult[0]->{'MAX(result)'}) ? '#e92f10' : '#008c3d';
                            }

                            $searchResults = get_field("search_courses", "options");

                            $askedQuery = "SELECT * FROM wp_asked_courses WHERE user_id = %s AND course_id = %s";
                            $askedQueryResult = $wpdb->get_results($wpdb->prepare($askedQuery, $_SESSION['ID'], $currentID));

                            $askedCheck = true;
                            if($askedQueryResult == null){
                                $askedCheck = false;
                            }

                            $link = "";
                            if($maxResult != "not-null-random-value"){
                                $link = get_permalink($currentID);
                            } elseif($askedCheck == false){
                                $link = $searchResults."?search-phrase=".$_GET['search-phrase']."&asked-user-id=".$_SESSION["ID"]."&asked-course-id=".$currentID;
                            } ?>
                            <a <?php if($link != "") echo "href=\"".$link."\""; ?> class="single-program-learning">
                                <div class="image d-flex justify-content-center align-items-center">
                                    <img src="<?php bloginfo("template_url"); ?>/images/programm-1.png">
                                </div>


                                <?php if($maxResult != "not-null-random-value" && $maxResult[0]->{'MAX(result)'} == null) { ?>
                                    <div class="card-content card-content-necessary card-content-warning d-flex justify-content-center">
                                        <div class="content-image">
                                            <img src="<?php bloginfo("template_url"); ?>/images/checkmark-warning.png">
                                        </div>
                                        <div class="content-text">
                                            Нужно пройти
                                        </div>
                                    </div>
                                <?php }
                                elseif($maxResult[0]->{'MAX(result)'} >= $passPercent && $maxResult[0]->{'MAX(result)'} != null) { ?>
                                    <div class="card-content d-flex justify-content-center">
                                        <div class="content-image">
                                            <img src="<?php bloginfo("template_url"); ?>/images/checkmark.png">
                                        </div>
                                        <div class="content-text">
                                            Пройдено
                                        </div>
                                    </div>
                                <?php }
                                elseif($maxResult[0]->{'MAX(result)'} < $passPercent && $maxResult[0]->{'MAX(result)'} != null) { ?>
                                    <div class="card-content card-content-warning d-flex justify-content-center">
                                        <div class="content-image">
                                            <img src="<?php bloginfo("template_url"); ?>/images/checkmark-warning.png">
                                        </div>
                                        <div class="content-text">
                                            Не пройдено
                                        </div>
                                    </div>
                                <?php }
                                elseif($maxResult == "not-null-random-value" && $askedCheck == false) { ?>
                                    <div class="card-content card-content-necessary card-content-warning d-flex justify-content-center">
                                        <div class="content-text">
                                            Запросить курс
                                        </div>
                                    </div>
                                <?php } elseif($askedCheck == true) { ?>
                                    <div class="card-content card-content-necessary card-content-warning d-flex justify-content-center">
                                        <div class="content-text">
                                            Запрошено. Ожидайте
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="card-text"><?php the_title(); ?></div>

                                <?php if($maxResult != "not-null-random-value") {
                                    if ($maxResult[0]->{'MAX(result)'} != null) { ?>
                                        <div class="card-result">Ваш результат: <span class="result-number" style="color: <?php echo $colorPercent; ?>"><?php echo round($maxResult[0]->{'MAX(result)'}, 1); ?>%</span></div>
                                    <?php }
                                } ?>

                            </a>
                            <?php $counterCourses++;
                        endwhile; else: endif; wp_reset_query();
                        if($counterCourses == 0) { ?>
                            <div class="no-posts">
                                Нет курсов по данному запросу !
                            </div>
                        <?php } ?>
                    </div>


                    <div class="pagination">
                        <?php
                        global $wp_query;
                        $big = 999999999; // need an unlikely integer
                        echo paginate_links( array(
                            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                            'format' => '?paged=%#%',
                            'current' => max( 1, get_query_var('paged') ),
                            'total' => $courses->max_num_pages,
                            'prev_text' => "<i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>",
                            'next_text' => "<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>",
                        ));
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>