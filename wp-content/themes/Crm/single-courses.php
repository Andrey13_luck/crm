<?php session_start();
if(!isset($_SESSION['ID'])) {
    wp_redirect( home_url() );
    die();
}
get_header();
if (have_posts()) : while (have_posts()) : the_post();

//     Questions, Single Page, shortcode
    function questions_func($atts) {
        $output = "";
        $questionsAll = get_field('questions_learning');
        $pathToTheme = get_template_directory_uri();
        $counter = 1;
        foreach($questionsAll as $value) {
            if($counter != $atts['number']){
                $counter++;
                continue;
            }
            $output .= "<form method='post' action='' class=\"test-content-wrapper\">";
            $output .= "<div class=\"test-title\">Самотестирование</div>";

            $output .= "<div class=\"answer-question\">{$value['learning_question']}</div>";

            $output .= "<div class=\"answer-wrapper\">";
            $answersCounter = 1;
            foreach($value['learning_variants'] as $item) {
                $output .= "<div class=\"single-checkbox d-flex align-items-center\">";
                $output .= "<input type=\"checkbox\" name=\"answer_{$answersCounter}\" class=\"form-checkbox\" id=\"check{$counter}_{$answersCounter}\" value=\"answer_{$answersCounter}\">";
                $output .= "<label class=\"check-label d-flex\" for=\"check{$counter}_{$answersCounter}\">";
                $output .= "<div class=\"check-custom-wrapper d-flex justify-content-center\">";
                $output .= "<div class=\"check-back\">";
                $output .= "<div class=\"check-checkmark\"><i class=\"fas fa-check\"></i></div>";
                $output .= "</div>";
                $output .= "</div>";
                $output .= "</label>";
                $output .= "<div class=\"check-title\">{$item['learning_answers']}</div>";
                $output .= "</div>";
                if($item['learning_answers_correct'] == true) {
                    $output .= "<input name=\"correct_{$answersCounter}\" type=\"hidden\" value=\"correct_{$answersCounter}\">";
                }
                $answersCounter++;
            }
            $output .= "</div>";

            $output .= "<div class=\"test-yourself-button\">";
            $output .= "<button type=\"submit\">Проверить себя</button>";
            $output .= "</div>";

            $output .= "<div class=\"right-answer\" style=\"display: none;\">";
            $output .= "<div class=\"image-answer\">";
            $output .= "<img src=\"\">";
            $output .= "</div>";
            $output .= "<div class=\"text\"><span class=\"\"></span></div>";
            $output .= "</div>";

            $output .= "</form>";
            $counter++;
        }

        return $output;
    }
    add_shortcode('question', 'questions_func');
//     END Questions, Single Page, shortcode

    global $wpdb;
    $currentID = get_the_ID(); ?>
    <div class="course-manag-page">
        <div class="main-container">
            <div class="main-wrapper d-flex flex-wrap justify-content-between">
                <?php require_once ("componentsPHP/sidebar.php");?>
                <div class="content">
                    <div class="title"><?php the_title(); ?></div>
                    <?php
                    $max = 0;
                    $sidebarBG = "";
                    $sidebarLine = "";
                    $passPercent = get_field("percent_pass");

                    $courseSlider = get_field("slider_presentation");
                    if($courseSlider != null){ ?>
                    <div class="main-content wysiwyg-wrapper">
                        <div id="courseSlider" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php $counterSlider = 1;
                                foreach ($courseSlider as $value){?>
                                    <div class="carousel-item <?php if($counterSlider == 1) echo "active"; ?>">
                                        <?= $value['single_slide']; ?>
                                    </div>
                                    <?php $counterSlider++;
                                } ?>
                            </div>
                            <ul class="carousel-indicators">
                                <?php $courseSliderIndicators = 0;
                                foreach ($courseSlider as $value) { ?>
                                    <li data-target="#courseSlider" data-slide-to="<?= $courseSliderIndicators; ?>" class="active"></li>
                                    <?php $courseSliderIndicators++;
                                } ?>
                            </ul>

                            <!-- Left and right controls -->
                            <a class="carousel-control-prev" href="#courseSlider" data-slide="prev">
                                <span class="carousel-control-prev-icon d-flex justify-content-center align-items-center"><i class="fas fa-angle-left"></i></span>
                            </a>
                            <a class="carousel-control-next" href="#courseSlider" data-slide="next">
                                <span class="carousel-control-next-icon d-flex justify-content-center align-items-center"><i class="fas fa-angle-right"></i></span>
                            </a>

                        </div>
                    </div>
                <?php } ?>


                    <div class="main-content wysiwyg-wrapper" style="padding: 30px 40px 30px;">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; else: endif;  wp_reset_query();
get_footer(); ?>