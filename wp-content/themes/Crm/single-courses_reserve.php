<?php session_start();
if(!isset($_SESSION['ID'])) {
    wp_redirect( home_url() );
    die();
}
get_header();
if (have_posts()) : while (have_posts()) : the_post();

//     Questions, Single Page, shortcode
    function questions_func($atts) {
        $output = "";
        $questionsAll = get_field('questions_learning');
        $pathToTheme = get_template_directory_uri();
        $counter = 1;
        foreach($questionsAll as $value) {
            if($counter != $atts['number']){
                $counter++;
                continue;
            }
            $output .= "<form method='post' action='' class=\"test-content-wrapper\">";
            $output .= "<div class=\"test-title\">Самотестирование</div>";

            $output .= "<div class=\"answer-question\">{$value['learning_question']}</div>";

            $output .= "<div class=\"answer-wrapper\">";
            $answersCounter = 1;
            foreach($value['learning_variants'] as $item) {
                $output .= "<div class=\"single-checkbox d-flex align-items-center\">";
                $output .= "<input type=\"checkbox\" name=\"answer_{$answersCounter}\" class=\"form-checkbox\" id=\"check{$counter}_{$answersCounter}\" value=\"answer_{$answersCounter}\">";
                $output .= "<label class=\"check-label d-flex\" for=\"check{$counter}_{$answersCounter}\">";
                $output .= "<div class=\"check-custom-wrapper d-flex justify-content-center\">";
                $output .= "<div class=\"check-back\">";
                $output .= "<div class=\"check-checkmark\"><i class=\"fas fa-check\"></i></div>";
                $output .= "</div>";
                $output .= "</div>";
                $output .= "</label>";
                $output .= "<div class=\"check-title\">{$item['learning_answers']}</div>";
                $output .= "</div>";
                if($item['learning_answers_correct'] == true) {
                    $output .= "<input name=\"correct_{$answersCounter}\" type=\"hidden\" value=\"correct_{$answersCounter}\">";
                }
                $answersCounter++;
            }
            $output .= "</div>";

            $output .= "<div class=\"test-yourself-button\">";
            $output .= "<button type=\"submit\">Проверить себя</button>";
            $output .= "</div>";

            $output .= "<div class=\"right-answer\" style=\"display: none;\">";
            $output .= "<div class=\"image-answer\">";
            $output .= "<img src=\"\">";
            $output .= "</div>";
            $output .= "<div class=\"text\"><span class=\"\"></span></div>";
            $output .= "</div>";

            $output .= "</form>";
            $counter++;
        }

        return $output;
    }
    add_shortcode('question', 'questions_func');
//     END Questions, Single Page, shortcode

    global $wpdb;
    $currentID = get_the_ID();
    $queryTests = "SELECT * FROM wp_tests_results WHERE user_id = %s AND course_id = %s";
    $testsResult = $wpdb->get_results($wpdb->prepare($queryTests, $_SESSION["ID"], $currentID)); ?>
    <div class="course-manag-page">
        <div class="main-container">
            <div class="main-wrapper d-flex flex-wrap justify-content-between">
                <?php require_once ("componentsPHP/sidebar.php");?>
                <div class="content">
                    <div class="title"><?php the_title(); ?></div>
                    <?php
                    $max = 0;
                    $sidebarBG = "";
                    $sidebarLine = "";
                    $passPercent = get_field("percent_pass");

                    if(empty($testsResult)) { ?>
                        <div class="task-block d-flex justify-content-center align-items-center">
                            <div class="image">
                                <img src="<?php bloginfo("template_url"); ?>/images/clock.png">
                            </div>
                            <div class="text">Нужно пройти</div>
                        </div>
                    <?php }

                    else { ?>
                        <div class="course-fail-block d-flex flex-wrap">
                            <div class="block-image">
                                <img src="<?php bloginfo("template_url"); ?>/images/success-cup.png">
                            </div>
                            <div class="block-content">
                                <?php foreach($testsResult as $value){
                                    if($value->result > $max) $max = $value->result;
                                }
                                $passPercent = get_field("percent_pass");
                                $sidebarBG = ($passPercent > $max) ? '#ffa798' : '#bbefd1';
                                $sidebarLine = ($passPercent > $max) ? '#e92f10' : '#008c3d';
                                $passedCheck = ($passPercent > $max) ? ' не пройден!' : ' пройден!'; ?>
                                <div class="block-title"><?php the_title(); echo $passedCheck; ?></div>
                                <div class="block-text">Количество попыток: <span class="block-attempt"><?php echo count($testsResult); ?></span></div>
                                <div class="progress" style="background-color: <?php echo $sidebarBG; ?>;">
                                    <div class="progress-bar progress-bar-striped" role="progressbar" style="width: <?php echo $max; ?>%; background-color: <?php echo $sidebarLine; ?>;"></div>
                                </div>
                                <div class="progress-result" style="color: <?php echo $sidebarLine; ?>">Ваш прогресс: <?php echo round($max, 1); ?>%</div>
                                <div class="result-title">История попыток</div>
                                <div class="table-wrapper">
                                    <?php $counterNumber = 0;
                                    $testsResult = array_reverse($testsResult);
                                    foreach($testsResult as $value) {

                                        $secondsRemaining = $value->time_remaining;
                                        $minutesTotal = get_field('time_to_pass');
                                        $finalSeconds = ($minutesTotal*60) - $secondsRemaining;

                                        $finalResultMinutes = floor($finalSeconds/60);
                                        $finalResultSeconds = $finalSeconds - $finalResultMinutes*60;

                                        if($counterNumber == 5) break;
                                        $resultColor = ($value->result >= $passPercent) ? "table-result-pass" : "table-result";
                                        $time1970 = strtotime($value->date);

                                        if($secondsRemaining != "end"){
                                            $finalResultSecondsRefactor = ($finalResultMinutes != null || $finalResultSeconds != null) ? ((strlen($finalResultSeconds) == 1) ? "0".$finalResultSeconds : $finalResultSeconds) : "";
                                            $finalResultToDisplay = ($finalResultMinutes != null || $finalResultSeconds != null) ? "Время: ".$finalResultMinutes.":".$finalResultSecondsRefactor : "";
                                        } else {
                                            $finalResultToDisplay = "Время истекло";
                                        } ?>
                                        <div class="table-line d-flex flex-wrap">
                                            <div class="single-table">№<?php echo count($testsResult) - $counterNumber; ?></div>
                                            <div class="single-table"><?php echo date("d.m.Y", $time1970); ?><br> <?=$finalResultToDisplay;?></div>
                                            <div class="single-table <?php echo $resultColor; ?>"><?php echo round($value->result, 1); ?>%</div>
                                        </div>
                                        <?php $counterNumber++;
                                    } ?>
                                </div>
                            </div>
                        </div>
                    <?php }


                    //                    require_once ("componentsPHP/course-title.php");
                    ?>

                    <div class="main-content wysiwyg-wrapper">

                        <?php $courseSlider = get_field("slider_presentation"); ?>
                        <div id="courseSlider" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php $counterSlider = 1;
                                foreach ($courseSlider as $value){?>
                                    <div class="carousel-item <?php if($counterSlider == 1) echo "active"; ?>">
                                        <?= $value['single_slide']; ?>
                                    </div>
                                    <?php $counterSlider++;
                                } ?>
                            </div>
                            <ul class="carousel-indicators">
                                <?php $courseSliderIndicators = 0;
                                foreach ($courseSlider as $value) { ?>
                                    <li data-target="#courseSlider" data-slide-to="<?= $courseSliderIndicators; ?>" class="active"></li>
                                    <?php $courseSliderIndicators++;
                                } ?>
                            </ul>


                            <!-- Left and right controls -->
                            <a class="carousel-control-prev" href="#courseSlider" data-slide="prev">
                                <span class="carousel-control-prev-icon d-flex justify-content-center align-items-center"><i class="fas fa-angle-left"></i></span>
                            </a>
                            <a class="carousel-control-next" href="#courseSlider" data-slide="next">
                                <span class="carousel-control-next-icon d-flex justify-content-center align-items-center"><i class="fas fa-angle-right"></i></span>
                            </a>

                        </div>
                    </div>


                    <div class="main-content wysiwyg-wrapper" style="border-top: none; padding: 30px 40px 30px;">

                        <?php the_content(); ?>

                        <div class="test-content-wrapper" style="margin: 0;">
                            <?php $testingPage = get_field("testing_page", "options");
                            if(empty($testsResult)) { ?>
                                <!--                                <div class="test-button">-->
                                <!--                                    <a href="">Самостоятельное пробное тестирование</a>-->
                                <!--                                </div>-->
                                <div class="test-button">
                                    <a href="<?php echo $testingPage; ?>?id_course=<?php echo $currentID; ?>">Экзаменационное тестирование</a>
                                </div>
                            <?php } else { ?>
                                <div class="test-start-button">
                                    <a href="<?php echo $testingPage; ?>?id_course=<?php echo $currentID; ?>">Начать тест</a>
                                </div>
                                <div class="result-test">Результат: <span class="result-test-score" style="color: <?php echo $sidebarLine; ?>"><?php echo round($max, 1); ?>%</span></div>
                            <?php } ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; else: endif;  wp_reset_query();
get_footer(); ?>