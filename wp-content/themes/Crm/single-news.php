<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="search-courses-single-page">
        <div class="top-block">
            <div class="top-block-light"></div>
            <div class="title"><?php the_title(); ?></div>
            <div class="date"><?php echo get_the_date("j.n.Y"); ?><span></span></div>
        </div>
        <div class="main-container">
            <div class="content-main-wrapper">
                <div class="content-wrapper wysiwyg-wrapper">
                    <?php the_content(); ?>
                <div class="button-back">
                    <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>">Назад</a>
                </div>
            </div>

        </div>
    </div>
   <?php  endwhile; else: endif; wp_reset_query(); ?>
<?php get_footer(); ?>