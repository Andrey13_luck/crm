<?php
/**
 * Template Name: About Us Page
 */
?>
<?php get_header() ; ?>
<?php // About Us -> Order Block -> shortcode
function order_block_function() {
    $output = "";
    $aboutInfo = get_field("about_us_blocks");
    $orderCounter = 1;
    foreach ($aboutInfo as $value) {
        if($orderCounter%2 == 0){
            $imageOrder = "about-us-image-order";
            $contentOrder = "about-us-content-order";
        } else {
            $imageOrder = "";
            $contentOrder = "";
        }
        $output .= "<div class=\"single-post d-flex flex-wrap justify-content-between align-items-center\">";
        if(!empty($value['about_image'])) {
            $output .= "<div class=\"image {$imageOrder}\">";
            $output .= "<img src=\"{$value['about_image']['sizes']['large']}\">";
            $output .= "</div>";
        }
        if($value['about_title'] != null || $value['about_text'] != null) {
            $output .= "<div class=\"content d-flex flex-wrap align-items-center {$contentOrder}\">";
            if($value['about_title'] != null) {
                $output .= "<div class=\"title\">{$value['about_title']}</div>";
            }
            if($value['about_text'] != null) {
                $output .= "<div class=\"text\">{$value['about_text']}</div>";
            }
            $output .= "</div>";
        }
        $output .= "</div>";
        $orderCounter++;
    }
    return $output;
}
add_shortcode('order_block', 'order_block_function');
// END About Us -> Order Block -> shortcode ?>
    <div class="about-us-page">
        <div class="main-container">
            <div class="main-title d-flex justify-content-between align-items-center"><?php the_title(); ?><span class="title-line"></span></div>
            <?php if (have_posts()) : while (have_posts()) : the_post();
                the_content();
            endwhile; else: endif; wp_reset_query(); ?>
        </div>
    </div>
<?php get_footer() ; ?>