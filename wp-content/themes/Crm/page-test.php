<?php
/**
 * Template Name: Test Page
 */

session_start();
if(!isset($_SESSION['ID'])) {
    wp_redirect( home_url() );
    die();
}
get_header(); ?>
    <div class="test-page">
        <div class="main-container">
            <div class="main-wrapper d-flex flex-wrap justify-content-between">
                <?php require_once ("componentsPHP/sidebar.php"); ?>
                <div class="content">
                    <div class="title">Тестирование</div>
                    <div class="text">
                        Тут располагаются обязательные тесты
                        не входящие ни в одну категорию обучения.
                    </div>
                    <div class="test-wrapper">
                        <?php global $wpdb;

                        $testingPage = get_field("testing_page","options");
                        $courseQuery = " SELECT * FROM wp_course_dependencies_tests WHERE user_id = %s ";
                        $coursesResult = $wpdb->get_results($wpdb->prepare($courseQuery, $_SESSION["ID"]));
                        $coursesIDs = [];
                        if (!empty($coursesResult)) {
                            foreach ($coursesResult as $value) {
                                array_push($coursesIDs, $value->course_id);
                            }
                        } else {
                            array_push($coursesIDs, "random-value");
                        }
                        $courses = new WP_Query(array("post_type" => "custom-courses", "posts_per_page" => -1, 'post__in' => $coursesIDs));
                        if ($courses->have_posts()) : while ($courses->have_posts()) : $courses->the_post();
                            $currentID = get_the_ID(); ?>
                            <div class="single-test">
                                <div class="test-title"><?php the_title(); ?></div>
                                <?php $passTill = get_field('pass_till');
                                if($passTill != null) { ?>
                                    <div class="test-date">Необходимо сдать до <?php echo $passTill; ?></div>
                                <?php }

                                $strtotimeFormat = strtotime($passTill);
                                $currentTime = time();

                                if($currentTime < $strtotimeFormat || $strtotimeFormat == null) { ?>
                                    <div class="test-button">
                                        <a href="<?php echo $testingPage; ?>?id_course=<?php echo $currentID; ?>">Начать Тестирование</a>
                                    </div>
                                <?php } else { ?>
                                    <div class="warning-time">
                                        Срок истёк. Вы не успели сдать
                                    </div>
                                <?php } ?>
                            </div>
                        <?php endwhile; else: endif; wp_reset_query();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>